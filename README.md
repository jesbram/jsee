# JSee

Este es un framework inspirado en django creado en express para nodejs 

# Instalacion

.. code-blocks:: bash

	git clone https://gitlab.com/jesbram/jsee.git

	git submodule init

	git submodule update

	#instalamos las dependencias de node 
	npm install

	#depues de clonar los submodulos tendremos cobra asi que procederemos
	#a instalar sus dependencias 
	cd cobra
	pip install -r requirements.txt 

	#retrocedemos a la raiz del sistema
	cd ..

	#iniciamos nuestro servidor 
	npm run dev

Dependiendo de las versiones que estemos manejando en diferentes partes del sistema es posible que queramos regenerar la documentacion ya que esta puede desvincularse de la documentacion de las apps de los sistemas que estemos desarrollando para esto hacemos lo siguiente:

	* Ejecutamos cobra con: npm run cobra
	* Seleccionamos la opción: 4) Construir documentación

Esto vinculara la documentación de las apps a la documentacion del sistema ademas de actualizar la documentacion propia del framework. Podemos acceder a la documentación a travez de la url http://localhost:3000/wiki


Creando usuario dependiendo de tu implementacion del sistema, usando las rutas por defecto de JSee seria lo siguiente: 

	* http://localhost:3000/register #para registrarse 
	* http://localhost:3000/login #para iniciar sesion
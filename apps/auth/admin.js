
module.exports = function (server,app) {

	const User =app.get_model_from_app("auth","User")
	const UserMeta =app.get_model_from_app("auth","UserMeta")
	const Group =app.get_model_from_app("auth","Group")

	app.register(User)
	app.register(UserMeta)
	app.register(Group)


}


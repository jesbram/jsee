const passport = require('passport');
const express = require('express');
const path = require('path');
const appRoot = require('app-root-path');
const jwt = require('jsonwebtoken');

module.exports = function (server,app,router) {
	var router = express.Router();
	var login_required=app.load_middlewares().login_required
	router.get("/",
		(req, res) => {
			res.json({"message":"soy un api"})
		})
	router.get("/test1",
		async (req, res) => {
			var model=require(path.join(appRoot.path,"lib/model.js"))
			var User=app.get_model("user")
			
			var model=model.ModelInstance(User)
		
			model.objects.all(function(err,set1){
				console.log("rrrrrr",set1[0])
				for (var elem  of set1){
					elem.permission.all(function(err2,set2){
						console.log("uuuu",set2[0])
					})
				}
			})
			
			res.json({"message":"soy un api de /test1"})
		})
	router.get("/token",
		login_required,
		async (req,res)=>{
			if (req.user){
				var payload = { user: req.user._id };
				app.do_signal("show_token_user",{"req":req,"res":res})
				
			}
			else{
				res.json({"message":"Acceso denegado"})
			}
		
		})


	router.post("/create",
		async (req,res)=>{
		if (req.user){
			var payload = { user: req.user._id };
			if (!req.body.email) 
			  {
			    return res.error(401)
			  } 
			app.do_signal("create_user_from_api",{"req":req,"res":res})
		
			app.do_signal("show_token_user",{"req":req,"res":res})

		}
		else{
			res.json({"message":"Acceso denegado"})
		}
		
	})
	
	return router
	

}
const passport = require('passport');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const path = require('path');
const { uuid } = require('uuidv4');
const appRoot = require('app-root-path');

const bodyParser = require('body-parser');
const settings = require('../../settings');
var csrf = require('csurf')

function login_required(req,res,next){
	if (req.isAuthenticated()){
		return next()
	}
	else{
		
		res.render(settings.login_view)
	}
}


function is_staff(req,res,next){

	if (req.isAuthenticated()){
		if (req.user.is_staff){
			return next()
		}
		else{
			res.redirect("/dashboard")
		}
		
		
	}
	else{
		
		res.render(settings.login_view)
		
	
	}
	

}
exports.middlewares=function (server,app) {
	
	require('../../config/passport')(passport)
	server.use(cookieParser())
	server.use(bodyParser.urlencoded({ extended: true }));
	server.use(session({
		key: 'sid', 
		secret: server.config.jwt_secret,
		resave: true, 
		saveUninitialized: true,  
		cookie: { maxAge: 60000 }}));
	server.use(passport.initialize());
	server.use(passport.session());
	var csrfProtection= csrf({ cookie: true })
	
	
	server.use(flash());
	

	
	// Utilidades
	server.use(function (req, res, next) {
	
		if (req.user){
			
			req.user.has_perm=function(perm){
				console.log("Otorgando permisos por prueba",perm)
				return true
			}
			req.user.has_role=function(role){
				console.log("Otorgando rol por prueba",role)
				return true
			}
			req.user.get_permissions=function(){
				console.log("Otorgando lista de permisos por prueba")
				return [] 
			}

			
		}

		res.error = function (status, msg, html) {

			res.status(status)

			if (msg instanceof Error) {
		      msg = msg.message
		    } 
		    else if (!msg) {
		    	switch (status) {
		    		case 401: msg = "Unauthorized"; break
		    		case 403: msg = "Forbidden"; break
		    		case 404: msg = "Not Found"; break
		    		case 500: msg = "Internal Error"; break
		    	}
		    }

		    if (html) {
		    	res.render(settings["view_"+status]?settings["view_"+status]:"error.ejs", {
		    		status: status,
		    		message: msg
		    	})
		    } 
		    else res.send(msg || "")
		}	
		res.login = function () {
		}
		next()
	})
	// Autentificación
	// necesaria en la visita
	if (settings.auto_csrf){
		server.use(csrfProtection)
	}
	server.use(async function (req, res, next) {
		const User = app.get_model_from_app("auth","User");
		
		var token = req.cookies.token || req.body.token
		if (token) {
			try {
				req.user = await User.findByToken(token)
			} catch (err) {
				console.error(err)
			}
		}

		if (settings.auto_csrf){
			res.locals["csrfToken"]=req.csrfToken()
		}
		else{
			res.locals["csrfToken"]=null
		}
		
		
		next()
	})


	

}

exports.login_required=login_required
exports.is_staff=is_staff
const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const path = require('path');
const appRoot = require('app-root-path')
const { string } = require('joi');

const schema = new mongoose.Schema({
  app_label: {type: String, required: true},
  model: {type: String, required: true},
  
});
var ContentType
schema.methods.get_for_model=function (model) {
  return this.find({"model":model.name})
}
schema.methods.toString=function(){
	return "ContentType["+this.id.slice(0,3)+"..."+this.id.slice(-5,-1)+"]"
}



ContentType = module.exports = mongoose.model('auth.ContentType', schema)


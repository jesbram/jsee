/*
Aqui se crean los grupos de permisos es decir roles que tendra un usuario
*/
const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')

const { string } = require('joi');

const schema = new mongoose.Schema({
  name: {type: String, required: true},
  permission: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "auth.Permission"},
  description: {type: String,},
});

var User

Group = module.exports = mongoose.model('auth.Group', schema)


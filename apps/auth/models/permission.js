/*
aqui es donde se van creando y asignando los permisos a los usuarios
*/

const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')

const { string } = require('joi');

const schema = new mongoose.Schema({
  name: {type: String, required: true},
  content_type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "auth.ContentType"
  },
  codename: {
    type: String,
    lowercase: true,
    required: true,
    index: true,
    unique: true,
  },
  
});

var Permission


schema.methods.create = function (perm) {
  
}

Permission = module.exports = mongoose.model('auth.Permission', schema)


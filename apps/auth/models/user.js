const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const path = require('path');
const appRoot = require('app-root-path')
const { string } = require('joi');

var Fields=require(path.join(appRoot.path,"lib/model.js")).Fields
var Methods=require(path.join(appRoot.path,"lib/model.js")).Methods

const schema = new mongoose.Schema(Fields("auth/user",{
  name: {type: String, required: true},
  user: {type: String, required: true,unique:true},
  email: {
    type: String,
    lowercase: true,
    required: true,
    index: true,
    unique: true,
  },
  password: {type:String},
  first_name:String,
  last_name:String,
  is_superuser:Boolean,
  is_staff:Boolean,
  is_active:Boolean,
  last_login:{
    type: Date
  },
  inclusive_access:{__datetime__:{type:Date,default:Date.now}},
  exclusive_access:{__datetime__:{type:Date,default:Date.now}},
  meta: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "auth.UserMeta"
  }],
  permissions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "auth.Permission"
  }],
  group: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "auth.Group"
  }],
  created: {
    type: Date,
    default: Date.now
  },
  reset_password_code: String, 
  
}));

var User


schema.methods.generateHash=function (password) {
  return  bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
}

schema.methods.set_password = function (password) {
  this.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
}


schema.methods.check_password = function (password) {
  if (!this.password) return false
  return bcrypt.compareSync(password, this.password)
}

schema.methods.get_token = function () {
  return jwt.sign(this.id, global.config.jwt_secret)
}

schema.methods.has_perm = function (user,perm) {
  /*
  

  var perm=perm.split(".")
  var app=perm[0]
  var codename=perm[1]
  var has_perm=false
  user.permissions.find({},function(err,data){
    for (var elem of data){
     ContentType.findById(elem["content_type"],function(err,data){
       if (data["app_label"]==app && data["codename"]==codename){
         has_perm=true
       }
     })
    }
  })

  return has_perm
  */
  return true

}
schema.methods.has_role = function (user,role) {
  /*
  for (var group of user.groups){
    if (group.name==role){
      return true
    }
  }
  return false
  */
  return true

}
schema.statics.get_meta = async function (user_id,name) {
  var usermeta

  var user=await User.findById(user_id).populate("meta")
  console.log(user)
  for (var meta of user.meta){
    if (meta["name"]==name){
      return meta
    }
  }


}
schema.statics.get_user_meta = async function (user_id,name) {
  var usermeta

  return await User.get_meta(user_id,name)["value"]
  


}
schema.statics.update_meta = async function (user_id,name,value) {

  /*
  User.find({"_id":user_id}, function(err, metas) {
    for (var meta of metas){
      UserMeta.findById(meta._id,function(err,metas)){

      }

    }
    });
  */
  var UserMeta=require("./usermeta")
  var meta=await User.get_meta(user_id,name)
  if (meta && meta["value"]!=undefined){
    console.log("xxxxxxxxxx",meta)
    var usermeta=(await UserMeta.findById(meta["_id"]))
    usermeta[name]=value
    await usermeta.save()
    console.log("aaaaaaa",usermeta)

  }
  else{
    
    var instance=await (new UserMeta({name:name,value:value}))
    await instance.save()
    
    var user=(await User.findByIdAndUpdate({"_id":user_id},{$push:{"meta":instance._id}}))
    console.log("dddddddd",user)
    
  }
}


schema.methods.has_event = function (id) {
  return this.inclusive_access.admin || (this.inclusive_access.events.indexOf(id) >= 0)
}

schema.methods.has_object = async function (object) {
  return this.inclusive_access.admin ||
    this.inclusive_access.objects.indexOf(object._id) >= 0 ||
    this.has_event((await object.get_event()).id)
}

schema.path('name').set(function (val) {
  this.name_changed = this.name
  return val
})


schema.statics.findByToken = async function (token) {
  try{
    var payload = jwt.verify(token.trim(), global.config.jwt_secret)
    if (typeof payload !== "string")
      throw new Error("Payload not a string")

    return await User.findById(payload)
  }
  catch(e){
    console.log(e)

  }
  
}

schema.statics.list = function () {
  return User.find({}, "name email inclusive_access exclusive_access permissions created").populate("last_visit")
}
Methods(schema)
User = module.exports = mongoose.model('auth.User', schema)


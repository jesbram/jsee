/*
Este modelo es usado para agregar cualquier metadado a un usuario 
caracteristicas extras, opciones etc
*/

const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')

const { string } = require('joi');

const schema = new mongoose.Schema({
  name: {type: String, required: true},
  value: {type: Object, required: true},
});

var UserMeta

UserMeta = module.exports = mongoose.model('auth.UserMeta', schema)


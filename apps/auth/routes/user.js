const passport = require('passport');
const settings = require('../../../settings');
const e = require('express');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const mail = require('../../../lib/mail');
const { error } = require('winston');
const user = require('../models/user');




module.exports = function (server,app,router) {

	/*Register */
	router.get('/register', (req, res) => {
		// Si se llama a esta función, la autenticación fue exitosa.
		// req.user` contiene al usuario autenticado
		res.render(settings.register_view?settings.register_view:"auth/register.ejs",
			{"settings":settings,
				"message":req.flash("signupMessage")})
	})

	router.post("/register", passport.authenticate("local-signup",{
		successRedirect:settings.register_redirect?settings.register_redirect:settings.login_redirect,
		failureRedirect:"/register",
		failureFlash:true,
	}),
	async (req, res) => {
		res.status(200).render("views/register.ejs")	
	})

	/*Login*/
	router.get("/login", async (req, res) => {
		res.render(settings.login_view?settings.login_view:"auth/login.ejs",
			{"message":req.flash("loginMessage")})
	})

	router.post("/login",async(req,res,next)=>{

		var redirect=settings.login_redirect
		if (req.body.redirect!=req.headers["origin"]+"/login"){
		 redirect=req.body.redirect;
		}
		

		passport.authenticate("local-login",{
		successRedirect:redirect,
		failureRedirect:req.body.redirect?req.body.redirect:settings.logout_redirect,
		failureFlash:true,
		})(req,res,next)
		
			
		
		
		
	})

	/*Logout */
	router.get("/logout",async (req, res) => {
		req.logout()
		req.session.destroy();
		res.clearCookie("token");
		//res.render("blank.ejs")
		res.redirect(settings.logout_redirect?settings.logout_redirect:"/"
			)
	})

	/*Perfil*/
	router.get("/profile", app.load_middlewares().login_required, async (req, res) => {
		res.render(settings.profile_view?settings.profile_view:"auth/profile.ejs",{
			user:req.user
		})
	})

	/*Password*/
	router.get('/resetPassword',async (req,res)=>{
		res.render(settings.resetPassword_view?settings.resetPassword_view:"auth/resetPassword.ejs")
	})

	router.post('/resetPassword',async (req,res)=>{
		const user = await User.findOne({email:req.body.email})
		if(!user.email) { 
			res.error(403, "Es obligatorio el email") 
		}
		var key=req.cookies.token
		var value=req.cookies.token		
		if (global["cache"][key]== value){
			req.user = await User.findOne({'email': Buffer.from(user.email, 'base64').toString('binary')})
			delete global["cache"][key]
		}  
		if(user) {
			console.log(user, "soy libresss")
			var code = Math.random().toString(36).substr(1)
			user.reset_password_code = code 		
			user.save()
			    
			req.user = user.email
			req.subject = 'Solicitud de reseteo de contraseña'
			req.msg = 'Se ha enviado un email a ' + user.email + ' para el reseteo de la contraseña'
			req.text = '<strong>Hola ' + user.name +'</strong><br>Estas recibiendo este correo electrónico para el reseteo de la contraseña de su cuenta ' + user.email+ '.<br>' +
				'Por favor haga click en el enlace o pegalo en el navegador para completar el proceso: ' +
				'<a href="http://' + req.headers.host + '/reset/' + code + '/' + user.email  + '">http://' + req.headers.host + '/reset/' + code + '/' + user.email + '</a><br>' +
				'<br>Si no realizó ninguna solicitud, por favor ignore este correo electrónico.'				
			await mail.sendEmail(req, res)			
		} else {
			res.error(403, "El email no se encuentra registrado")
		}
	})

	router.get('/reset/:code/:email',async (req,res)=>{
		res.render(settings.reset_view?settings.reset_view:"auth/reset.ejs")
	})

	router.post('/reset/:code/:email', async function (req, res) {
		console.log(req.body, "algo tenemos", req.params)
		if(!req.params.code && !req.body.email) { res.error(403, "Es obligatorio el código de validación y el email") }
		
		var user = await User.findOne({'email': req.body.email, 'reset_password_code': req.params.code})
		if(user) {
		  user.set_password(req.body.password)
		  var code = Math.random().toString(36).substr(1)
	  
		  user.reset_password_code = code 
		  user.save()
		//   res.status(200).json({status: 200, sucess: true, msg: 'Se reseteo correctamente la contraseña / Your password has been successfully changed'})
		  return res.render(settings.login_view?settings.login_view:"auth/login.ejs")		 
		} else {
		  res.error(403, "El código ya caducó, vuelva a realizar la solicitud / This code has already been used")
		}
	})
	return router;
}
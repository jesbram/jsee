const jwt = require('jsonwebtoken');


module.exports = (function (server,app) {
	const User=app.get_model_from_app("auth","User")

	app.add_signal("create_user_from_api",async function(data){
		let req=data["req"]
		let res=data["res"]
		var user = await User.findOne({'email': req.body.email})
		if (user.password) {
	        return res.error(400, "Usurio ya existe")
	      } else {
	        var custom = req.body.custom
	        if (custom) {
	          for (var k in custom) {
	            user.custom.set(k, custom[k])
	          }
	        }
	        msg = "Creating access token for " + user.name
	      }

		if (req.user && req.user.has_perm("create_users")){//si esta autenticado
			user = new User({
		        name: req.body.name,
		        email: req.body.email,
		        inclusive_access:{
		          admin: req.body.inclusive_access.admin,
		          events: req.body.inclusive_access.events,
		        },
		        custom: req.body.custom
		      })
		      
		      user.set_password(req.body.password)
		      try { await user.save() }
      		  catch (e) { return next(e) }

		}
		
			
      

	})

	app.add_signal("show_token_user",async function(data){
		let req=data["req"]
		let res=data["res"]

		var token=await req.user.get_token()
		
		res.status(200).send({ token });

	})
	
})

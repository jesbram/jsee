<!-- 
* Agregada barra de navegación 
* 
-->
# Nueva pagina de inicio con sistema de nuevas caracteristicas
![Barra de navegacion](/static/dashboard/imgs/news.jpg)

Resalta los nuevos desarrollos que estan disponibles en el sistema mediante tu app gracias al sistema de nuevas caracteristicas, estas se veran en la pagina de Bienvenida del framework solo necesitas un archivo NEWS.md  

# Añadida barra de navegación para el administrador de modelos 
![Barra de navegacion](/static/dashboard/imgs/navbar.jpg)

Ahora podras hacer mas rapida las busquedas en la documentación usando el campo de busqueda de la barra de navegacion 



const passport = require('passport');
const express = require('express');
const path = require('path');
const appRoot = require('app-root-path');

module.exports = function (server,app,router) {
	var router = express.Router();

	router.route("/pagination/:app/:model/page=:page/:limit?")
		.get(async (req, res) => {
			var model=require(path.join(appRoot.path,"lib/model.js"))

			model=app.get_model_from_app(req.params.app,req.params.model)

			let data = []
			//({}, function(err, docs)
			data = await model.find()

			let num_page = parseInt(req.params.page) || 1
			let per_page = req.params.limit || data.length
			let to_page = (num_page * per_page)
			let from_page = (num_page * per_page) - per_page
			let items_per_page = data.slice(from_page, to_page)

			res.json(items_per_page)
		})

		router.route("/search/:app/:model/:id")
		.get(async (req, res) => {
			var model=require(path.join(appRoot.path,"lib/model.js"))

			model=app.get_model_from_app(req.params.app,req.params.model)

			let data;
			//({}, function(err, docs)
			data = await model.findById(req.params.id)

			res.json(data)
		})

		router.route('/update/:app/:model/:id')
		.put(async (req, res) => {
			var model=require(path.join(appRoot.path,"lib/model.js"))
			model=app.get_model_from_app(req.params.app,req.params.model)

			await model.findOneAndUpdate({_id: req.params.id}, req.body)

			res.json({"msg": "updated"})
		})

		router.route('/delete/:app/:model/:id')
		.delete(async (req, res) => {
			var model=require(path.join(appRoot.path,"lib/model.js"))
			model=app.get_model_from_app(req.params.app,req.params.model)

			await model.findOneAndDelete({_id: req.params.id})

			res.json({"msg": "deleted"})
		})
	
	return router
	

}
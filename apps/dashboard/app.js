const AppConfig=require("../../lib/apps").AppConfig
class App extends AppConfig{
	constructor(name,app){
		super(name,app)
		this.settings.compile={
			"main":"static/js/main.js",
			"forms":"static/dashboard/js/forms.js"
			}//#compila las 
		
	}
	ready(){
		var that=this
		var fn=async function(){
		
			await that.get_model_from_app(that.name,"Option").init_options([
					["show_jsee_filters",false],
					["debug",true]
				]
				)

	
		}
		fn()
	}
	get_cookies_settings(req){
		cookie=JSON.parse(Buffer(req.cookie("jsee_settings"), 'base64').toString('ascii'))
		return cookie
	}
	set_cookies_settings(req,res,name,value){
		cookie=this.get_cookies_settings(req)
		cookie[name]=value
		
		cookie=Buffer(JSON.stringify(cookie)).toString('base64')
		res.cookie("jsee_settings",cookie)
	}
	async get_option(name,_default){
		var Option=this.get_model_from_app(this.name,"Option")

		var option=await Option.get_option(name)

		if (option!=undefined){
			return option
		}
		else{
			return _default
		}

	}
	async update_option(name,value){
		var Option=this.get_model_from_app(this.name,"Option")
		await Option.update_option(name,value)

	}
		
	
}
module.exports=App
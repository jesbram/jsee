const fs=require("fs")
const path = require('path');
const appRoot = require('app-root-path');
module.exports=function (server,app) {
	app.add_filter("head",function(html,data){
		if (!html){
			html=""

		}
		html+="<link rel='stylesheet' href='/static/css/bootstrap.css'/>"
		html+="<link rel='stylesheet' href='/static/css/bootstrap-select.min.css'/>"

	
		return html
	})
	
	app.add_filter("head",function(html,data){
		if (!html){
			html=""
		}
		if (app.get_settings().javascript_variables){

			html+="<script>window.settings="+JSON.stringify(app.get_settings().javascript_variables)+"</script>"
		}
		
		return html
	})

	app.add_filter("footer",function(html,data){
		if (!html){
			html=""
		}
		html+="<script src='/static/js/jquery-3.5.1.min.js'></script><script src='/static/js/bootstrap.min.js'></script>"
		html+="<script src='/static/js/bootstrap-select.min.js'></script>"
		return html
	})
	app.add_filter("jsee_news",function(html,data){
		if (!html){
			html=""
		}
		for (var _app of app.get_settings().apps){ 
	
			//Esta clase de archivos es usados para ampliar los campos de los esquemas del modelo
			
			if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/NEWS.md'))){
				
				var archivo=fs.readFileSync(path.join(appRoot.path,"apps/"+_app+'/NEWS.md'), 'utf-8');
				var showdown  = require('showdown');
			    var converter = new showdown.Converter();
			    html      += converter.makeHtml(archivo);
				
			}
		}
		return html
	})
	


}
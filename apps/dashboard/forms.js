const path = require('path');
const appRoot = require('app-root-path');
var Apps=require(path.join(appRoot.path,"lib/apps.js"))
var signals=require(path.join(appRoot.path,"lib/signals.js"))
var forms=require(path.join(appRoot.path,"lib/forms.js"))
var User=Apps.get_app("auth").get_model("user")


class PruebaForm extends forms.ModelForm{
	constructor(instance,data){
		super(instance,data,{
			model:User,
			fields:"__all__",
			widgets:{"password":new forms.TextInput({"type":"password"})}
		})
		
	}
}

class Prueba2Form extends forms.Form{
	constructor(instance,data){
		super(instance,data,{
			model:User,
			fields:"__all__",
		})
		this.fields["campo1"]=new forms.CharField("Campo1",new forms.TextInput())
		
	}
}


class Prueba3Form extends forms.Form{
	constructor(instance,data){
		super(instance,data,{
			model: User,
			fields: '__all__'
		})
		this.fields['permission1'] = new forms.CharField('Permission1', new forms.MultipleChoiceWidget({":data":[{label:"Etiqueta1",key:0,initial:"etiqueta1"},{label:"Etiqueta2",key:1,initial:"etiqueta2"}]}))
		this.fields['permission2'] = new forms.CharField('Permission2', new forms.MultipleChoiceWidget({":data":[{label:"Etiqueta3",key:0,initial:"etiqueta3"},{label:"Etiqueta4",key:1,initial:"etiqueta4"}]}))
	}
}

class Prueba4Form extends forms.Form{
	constructor(instance,data){
		super(instance,data,{
			model:User,
			fields:"__all__",
		})
		this.fields["campo1"]=new forms.ChoiceField("Campo1",new forms.Select({":options":[
			{"label":"Hola","value":0},
			{"label":"Hola2","value":1}],
			"multiple":"true"
		}))
		this.fields["campo2"]=new forms.CharField("Campo2",new forms.ColorInput({}))
		this.fields["campo3"]=new forms.CharField("Campo3",new forms.Preview({}))
		this.fields["campo4"]=new forms.CharField("Campo4",new forms.RichText({}))
		this.fields["campo5"]=new forms.CharField("Campo5",new forms.TextInput({"type":"file"}))
		
	}
}


class CrudForm extends forms.ModelForm{
		constructor(instance,data,model){
			
			super(instance,data,{
				model:model,
				fields: "__all__",
			})		
			signals.do_signal("crudform",{form:this,model:model})
			
		}
		
	}
exports.CrudForm=CrudForm
exports.PruebaForm=PruebaForm
exports.Prueba2Form=Prueba2Form
exports.Prueba3Form=Prueba3Form
exports.Prueba4Form=Prueba4Form
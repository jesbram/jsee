const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')

const { string } = require('joi');
const { text } = require('body-parser');

const schema = new mongoose.Schema({
  name: {type: String, required: true},
  value: {type: Object, required: true,
  },
});

var Option


schema.path('name').set(function (val) {
  this.name_changed = this.name
  return val
})

schema.statics.get_option=async function(name){
	value=(await Option.findOne({"name":name}))
	if (value){
		return value["value"]
	}
	
}
schema.statics.init_options=async function(data){
	for (var elem of data){
		
		await Option.update_option(elem[0],elem[1])
	}
	
}

schema.statics.update_option=async function(name,value){
	if ((await Option.get_option(name))!=undefined){
		await Option.updateOne({"name":name},{$set:{"value":value}})
	}
	else{
		
		instance=await (new Option({name:name,value:value}))
		
		await instance.save()
	}
	
}


Option = module.exports = mongoose.model('dashboard.Option', schema)

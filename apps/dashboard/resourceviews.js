const path = require('path');
const appRoot = require('app-root-path');
const ResourceView =require(path.join(appRoot.path,"lib/ResourceView.js")).ResourceView
var Apps=require(path.join(appRoot.path,"lib/apps.js"))
var libforms=require(path.join(appRoot.path,"lib/forms.js"))
var forms=require("./forms")

class CrudView extends ResourceView{
	
	constructor(base_slug,router,app){
		super(base_slug,router,app,{
			model:function(self,req){
				return self.app.get_model_from_app(req.params.app,req.params.model)
			},
			form:forms.CrudForm,
			middleware:app.get_app("auth").load_middlewares().is_staff
		})
	}

	filter_data(name,data){

			if (name=="create" || name=="edit"){
				
				if (data["form"].fields.hasOwnProperty("description")){
					
					var form=new libforms.TextArea({name:"description"})
			
					data["form"].fields["description"].widget=form
				}
			}
		}

	exclude_fields(name,req,res){
		if (name=="index"){
			if (req.params.model=="User" && req.params.app=="auth"){
		

				return ["password"]
			}
			
		}
		return []
	}
	format_field(req,name,value){
		var html=""
		if (value==undefined){
			value=""
		}
		if (req.params.model=="User" && req.params.app=="auth"){
			if (name=="inclusive_access" || name=="exclusive_access"){
				var data=value.toObject()
				for (var key of Object.keys(data)){
				
					if (!(key.startsWith("__") && key.endsWith("__")) && !key.startsWith("$")){
						html+=key+": "+data[key]+"<br>"
					}
					
				}
			}
			else{
				
				html+=value
			}
		}
		else{
			html+=value

		}
		return html

	}

}

exports.CrudView=CrudView
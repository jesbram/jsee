const express = require('express');

const path = require('path');
const appRoot = require('app-root-path');

module.exports = function (server,app,router) {
	var router = express.Router();
	var views = app.load_source("resourceviews.js")
	var login_required=app.get_app("auth").load_middlewares().login_required
	var is_staff=app.get_app("auth").load_middlewares().is_staff

	router.get('/', 
		login_required,
		(req, res) => {
			// Si se llama a esta función, la autenticación fue exitosa.
			// req.user` contiene al usuario autenticado

		models=app.get_all_models_registered()
		var filters=require(path.join(appRoot.path,"lib/filters.js"))
		console.log("ssssssssss",filters.filters)
		res.render('dashboard/apps.ejs',{app:app.name,models_registered:models,is_staff:req.user.is_staff});

		})

	

	router.get('/test', 
		(req, res) => {
		signals=require(path.join(appRoot.path,"lib/signals.js"))
		signals.do_signal("prueba")
		res.render('dashboard/example.ejs',{app:app.name});
		})
	router.get('/test2', 

		(req, res) => {

		res.render('dashboard/example2.ejs',{app:app.name});
		})
	router.get('/test3', 
		(req, res) => {
		var forms=app.get_forms()
		res.render('dashboard/form.ejs',{app:app.name,form:new forms.PruebaForm()});
		})
	router.get('/testform', 
		(req, res) => {
		var forms=app.get_forms()
		res.render('dashboard/form.ejs',{app:app.name,form:new forms.Prueba4Form()});
		})
	router.post('/testform', 
		(req, res) => {
			console.log(req.body)
			console.log(req.files)
			console.log(req.file)
		})
	router.get('/test4', 
		(req, res) => {
			// Si se llama a esta función, la autenticación fue exitosa.
			// req.user` contiene al usuario autenticado
		var forms=app.get_forms()
		res.render('dashboard/multipleselect.ejs', {app:app.name, form:new forms.Prueba3Form()});
		})

	router.get('/test5', 
		(req, res) => {

		var forms=app.get_forms()
		res.render('dashboard/form.ejs',{app:app.name,form:new forms.Prueba2Form()});
		})
	router.get('/test6', 
		(req, res) => {
		var User=app.get_model_from_app("auth","User")
		User.test_method = function () {
			console.log("soy un metodo de prueba")
		}
		User.test_method()
		res.send("Esto es una prueba")
		})


	router.get('/:app', 
		is_staff,
		(req, res) => {
			// Si se llama a esta función, la autenticación fue exitosa.
			// req.user` contiene al usuario autenticado
		
		
		var _models=app.get_all_models_registered()
		
		for (var _app in _models){

			if (_app==req.params.app){

				models={}
				models[_app]=_models[_app]
				
				break
			}
		}
		res.render('dashboard/apps.ejs',{app:app.name,app_model:_app,models_registered:models,is_staff:req.user.is_staff});
		})
	new views.CrudView('/:app/:model',router,app)

	router.get('/:app/:model/:page', 
		(req, res) => {
		
		model=app.get_model_from_app(req.params.app,req.params.model)
		num_page = parseInt(req.params.page)
		skip_page = (num_page-1)*20
		model.count().then(function ( count ){
			num_pages = parseInt((count/20)+1);
		});

		rows=model.find({ is_active: true },{username:1, personal_info:1})
		.skip(skip_page)
		.limit(20)
		.lean()

		var rows=[]
		var fields=[]
		res.render('dashboard/model.ejs',{app:app.name,rows:rows,
			fields:fields,
			num_page: num_page,
			num_pages: num_pages
		})})
	router.get('/:app/:model/:id/edit', 
		//passport.authenticate('local'),
		(req, res) => {
			// Si se llama a esta función, la autenticación fue exitosa.
			// req.user` contiene al usuario autenticado
		model=app.get_model_from_app(req.params.app,req.params.model)
		model.find({$id:req.params.id})
		form=new FormFromModel(model)
		res.render('dashboard/model.ejs',{app:app.name,form:form});
		})
	router.post('/test4', 
		(req, res) => {
			// Si se llama a esta función, la autenticación fue exitosa.
			// req.user` contiene al usuario autenticado
		var forms=app.get_forms()
		var form=new forms.Prueba3Form(req)
		res.render('dashboard/multipleselect.ejs', {app:app.name,});
		})
	router.put('/test4/:id', 
		async (req, res) => {
			// Si se llama a esta función, la autenticación fue exitosa.
			// req.user` contiene al usuario autenticado
		var forms=app.get_forms()
		var Option=app.get_model_from_app("dashboard","Option")
		await Option.findById(req.params.id,function(err,instance){
			var form=new forms.Prueba3Form(req,instance)
			form.save()

		})
		
		res.render('dashboard/multipleselect.ejs', {app:app.name,});
		})

	/*
	Esta ruta esta pensada para renderizar las vistas que deban ser servidas
	como por ejemplo las de vue
	*/
	router.get('/views', 
		(req, res) => {
		res.render('dashboard/example.ejs',{app:app.name});
		})

	
	return router
}
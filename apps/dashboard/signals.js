const path = require('path');
const appRoot = require('app-root-path');
var forms=require(path.join(appRoot.path,"lib/forms.js"))
module.exports=function (server,app) {

	app.add_signal("crudform",function(data){
		if (data["model"].modelName=="auth.User"){

				data["form"].fields["password"].widget=new forms.TextInput({"type":"password","name":"password"})
			}
	})
	app.add_signal("load_route",async function(data){
		var filters=require(path.join(appRoot.path,"lib/filters.js"))
		
		value=await app.get_option("show_jsee_filters",false)
		if (value){

			for (var elem in filters.filters){
				filters.filters[elem][0]=[function(html,data){
					if (value){
				 return "["+elem+"]"}}]
		
			}
			
		}
	
		
	})

}
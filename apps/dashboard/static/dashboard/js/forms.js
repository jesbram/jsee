import Vue from 'vue-full'
import Element from 'element-ui'
import VueCodemirror from 'vue-codemirror'

import "codemirror/lib/codemirror.css"
// import language js

import 'codemirror/mode/javascript/javascript.js'

// import theme style
import 'codemirror/theme/base16-dark.css'
import "vue-select/dist/vue-select.css";
import "element-ui/packages/theme-chalk/lib/index.css"
// import language js

// import theme style
import Select from "../../components/select.vue"
import ColorPicker from "../../components/color.vue"
import Preview from "../../components/preview.vue"
//import CKEditor from '@ckeditor/ckeditor5-vue'

//Vue.use(CKEditor)
//import editor from "../../components/ckeditor.vue"

import tiny from "../../components/tinymce.vue"



console.log(VueCodemirror)
Element.locale("es")
Vue.use(Element)
Vue.use(VueCodemirror)


/*
si se desea tener acualizacion de opciones via ajax este complemento
debe tener un atributo remote="{get_option_url}"
*/
Vue.component("form-select",{
  template:"el-select"
})

Vue.component("rich-text",{
  extends:tiny
})


Vue.component("inputcodemirror",{
  props:["code","field"],
  template:' <codemirror v-model="code" :name="field" :options="editorOptions">{{field_value}}</codemirror>',
  data () {
    return {
      editorOptions: {
        tabSize: 4,
        mode: 'text/javascript',
        theme: 'base16-dark',
        lineNumbers: true,
        line: true,
        // more CodeMirror options...
      }
    }
  },
  methods: {
    onCmReady(cm) {
      console.log('the editor is readied!', cm)
    },
    onCmFocus(cm) {
      console.log('the editor is focused!', cm)
    },
    onCmCodeChange(newCode) {
      console.log('this is new code', newCode)
      this.code = newCode
    }
  },
  computed: {

    field(){
      return this.$attrs["name"]
    },
    field_value(){
      return this.$attrs["value"]
    }

  },
  mounted() {
    this.code=this.$attrs["value"]
    console.log('the current CodeMirror instance object:')
    // you can use this.codemirror to do something...
  }
 })

 Vue.component("multiple-choice-widget",{
  props:["filterMethod"],
  filterMethod(query, item) {
    return item.initial.toLowerCase().indexOf(query.toLowerCase()) > -1;
  },
  right_checked:null,
  data(){
      return {content:JSON.parse(this.$attrs.value),sorbtale:true,name:"name"}
    
  },
  template:`
  <div class='multiple-choice-widget' >
  <input :name='name' data-input hidden>
  <el-transfer filterable :filter-method='filterMethod' filter-placeholder='Search keyword' @change='change' v-model='content' :data='data' ></el-transfer>
  <div class='multiple-choice-widget-btns' style="max-width: 65px">
    <button  class='btn btn-success btn-add' type='button' @click="add" >+</button>
    <button class='btn btn-danger btn-remove' type='button' @click="remove">-</button>
    <span v-if="sorbtale">
     <button  class='btn btn-primary btn-add' style='width:100%' type='button' @click="up" >Up</button>
     <button class='btn btn-primary btn-remove' style='width:100%' type='button' @click="down">Down</button>
    </span>

   
  </div>
  </div>
  `,
  methods:{
    add(){
        //window.open("_blank",event.target.attributes["href"])
        var that =this
        this.$alert('<input name="name"><input name="value">', 'Nuevo elemento', {
          confirmButtonText: 'OK',
          dangerouslyUseHTMLString: true,
          callback: (action,instance) => {

            
            var name=instance.$el.querySelector("[name='name']").value
            var value=instance.$el.querySelector("[name='value']").value
           
            that.data.push({
              label: name,
              key:value,
              innitial:value,
            })
            console.log(that.data)
            var node=that.$el.querySelector(".el-transfer__buttons [disabled]")
            node.disabled=false
            node.click()
            node.disabled=true
            console.log(that.$el.querySelectorAll(".el-checkbox__inner"))
            that.$el.querySelectorAll(".el-checkbox__inner")[1].checked=false 
                  

          /*
          that.$el.querySelectorAll(".el-transfer-panel")[0].querySelector(".el-checkbox-group").appendChild(node)    
          node.outerHTML=`<label class="el-checkbox el-transfer-panel__item ">
            <span class="el-checkbox__input"><span class="el-checkbox__inner"></span>
            <input type="checkbox" class="el-checkbox__original" value="${value}"></span><span class="el-checkbox__label">
            <span>${name}</span></span></label>`
          console.log(that.$el.querySelectorAll(".el-transfer-panel")[0].querySelector(".el-checkbox-group").childNodes)
          */
          }
          
        });

    },
    remove(){


    },
    up(){

    },
    down(){

    },
    async change(value){
      
      //si esta activado el modo 3
  
      if (this.$root.$el.hasAttribute("updater")){
        var data={}
        
        var url=this.$el.hasAttribute("action")
        var that=this
        if (this.$el.hasAttribute("action")==""){
          url="/json"+location.pathname
        }

        data[this.$attrs.name]=value
       
        fetch(url,{
              method:'PATCH',
              headers:{
                'Content-Type': 'application/json'
              },
              mode: 'cors',
             cache: 'default' ,
              body:JSON.stringify(data),
          }).then(function(response) {
          return response.json();
        })
        .then(function(data){
          
          if (that.$el.updater){
            that.$el.updater(data)
          }
        })
        

      }
      
      //actualiza el input para ser usado en los modos 1 y 2
      this.$el.querySelector("[data-input]").value=value

    }

  },
  computed:{
    data(){
     
      return this.$attrs.data
    },
  
  },
  mounted(){
    //var value=JSON.parse(this.$attrs.value)
    console.log("@@@@@@@2",this.$attrs.name)
    this.name=this.$attrs.name



  
  },

})


window.JSEE_FORM=function(selector){
  return new Vue({
  el:selector,
  components:{"form-select":Select,"color-picker":ColorPicker,"preview":Preview,
  "rich-text":tiny
  },
  data:function(){
    return{
        data:[]
    }
  },

  methods:{
    async submit(message){
      //Esto es el modo 2
      var form=new FormData(this.$el)
    
      var that=this
      var url=this.$el.getAttribute("action")
      if (this.$el.getAttribute("action")==""){
            url="/json"+location.pathname
          }
     

      fetch(url,{
            method:that.$el.getAttribute("method"),
            headers:{
              'Accept': 'application/JSON' ,
              'Content-Type':'application/json'
            },
            body: JSON.stringify(Object.fromEntries(form)),
            contentType: false,
            processData: false,
            
        }).then(function(response) {
        return response.json();
      })
      .then(function(data){
        if (that.$el.do){
          that.$el.do(data)
        }
      }
      );
  },
  }
  })


}

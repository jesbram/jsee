const express = require('express');
module.exports = function (server,app,router) {
	var router = express.Router();

	router.get("/",
		(req, res) => {
			res.json()
		})
	router.patch("/change-lang",
		(req, res) => {
			
			app.get_app("dashboard").set_cookies_settings(req,res,"lang",req.body.lang) 
			
			res.json({"message":"Lenguaje cambiado"})
		})
	
	
	return router
	

}

const path = require('path');
const appRoot = require('app-root-path');
const settings = require('../../settings');
const fs=require("fs")
var lang
function translate(text,option){
	
	
	if (option==undefined){
		lang=settings.lang
	}
	function _(_path,text){
		console.log(_path,fs.existsSync(_path))
		if (fs.existsSync(_path)){
			var data=require(_path)
			if (data[text]){
				return data[text]
			}

		}

	}
	function trans(text,lang){
		
		
		
		for (elem of settings.apps){
			
			var _path=path.join(appRoot.path,"apps/"+elem+"/i18n/"+(settings.base_lang?settings.base_lang:"en")+"-"+lang+".json")
			var t=_(_path,text)
			if (t){
				return t
			}
		}

		var _path=path.join(appRoot.path,"public/media/i18n/"+(settings.base_lang?settings.base_lang:"en")+"-"+lang+".json")
		if (fs.existsSync(_path)){
			return _(_path,text)
		}
		

	}

	if (typeof (option)=="string"){
		lang=option
		var t=trans(text,lang)
		if (t){
			return t
		}
		

	}
	else if (typeof (option)=="object"){
		if (settings.apps.indexOf(option.ctx)>-1){
			lang=option.lang
			var _path=path.join(appRoot.path,"apps/"+option.ctx+"/i18n/"+settings.base_lang?settings.base_lang:"en"+"-"+lang+".json")
			var t=_(_path,text)
			if (t){
				return t
			}
		}
		
	}
	else{

		var t=trans(text,lang)
		if (t){
			return t
		}
	}
	return text
	
}
exports.middlewares=function (server,app) {
	
	server.use(function(req,res,next){
		res.locals["_"]=translate
		next()
	})
	server.use(function(req,res,next){
		if (req.query.lang){
			lang=req.query.lang
		}
		next()
	})


}

exports.translate=translate
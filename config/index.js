
const fs = require("fs");
const path = require('path');
const appRoot = require('app-root-path');

if (!fs.existsSync(path.join(appRoot.path, "config.json")))
  fs.copyFileSync("config.example.json", "config.json");



const parameterConfig = require(path.join(appRoot.path, "config.json"));
const env="development";
module.exports=parameterConfig[env];
import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      nombre: Joi.string().required(),
      apellido: Joi.string().required(),
    }
  },
  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      nombre: Joi.string().required(),
      apellido: Joi.string().required(),
    },
    params: {
      userId: Joi.string().hex().required()
    }
  }
};

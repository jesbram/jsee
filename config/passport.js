const LocalStrategy = require('passport-local').Strategy;
const User = require('../apps/auth/models/user');
const jwt = require('jsonwebtoken');
config=require("./index")
module.exports = function (passport) {

  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize user
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  // Signup
  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function (req, email, password, done) {
    User.findOne({'email': email}, function (err, user) {
    
      if (err) {
        return done(err);
      }
      if (user) {
        return done(null, false, req.flash('signupMessage', 'El email ya existe'));
      } else {
        var newUser = new User();
       
        newUser.user = req.body.user;
        newUser.name = req.body.user;
        newUser.email = req.body.email;
        newUser.password = newUser.generateHash(password);

        newUser.save(function (err) {
    
          if (err) { throw err; }
          
          const token = jwt.sign(newUser.toJSON(), config.jwt_secret);
          return done(null, newUser, token, newUser.email);
        });
      }
    });
  }));

  // login
  passport.use('local-login', new LocalStrategy({
    usernameField: 'user',
    passwordField: 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  }, 
  function (req, email, password, done) {
    
    User.findOne({$or:[{"email":req.body.user},{"user":req.body.user}]}, function (err, user) {
      
      if (err) { 
        return done(err); 
      }
      if (!user) {
        return done(null, false, req.flash('loginMessage', 'Usuario no encontrado'))
      }
      if (!user.check_password(password)) {

        return done(null, false, req.flash('loginMessage', 'La Contraseña no es valida'));
      }

      return done(null,user);
    });
  }));
}
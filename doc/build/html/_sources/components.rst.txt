
Componentes
==========================================

Un componente se refiere a un elemento de una interfaz visual con la que interacciona el usuario, generalmente este esta hecho en vue. El sistema de componentes esta fuermetemente ligado a el motor de formularios ya que la mayoria de componentes son usados para representarcampos de formularios.


Como crear/integrar un componente
------------------------------------------------
Por regla general los componentes se crean dentro de la carpeta static/components de cada app segun la estructura que se este implementando a esta se le añade una carpeta para indicar el nombre de la aplicacion a la que va dirigida el componente quedando algo como por ejemplo **static/components/<app>/** donde **<app>** puede ser el nombre de la aplicacion actual o de otra aplicacion. El uso de la carpeta components es para indicar que alli de colocarian los archivos de **.vue** o con una estructrua modular de diseño de componentes, esto quiere decir que el archivo .js al que llamaremos cargador el cual en el caso vue es donde se instancia la aplicacion vue estara alojada fuera de la carpeta components es decir en las siguientes rutas de la applicacion jsee **static/js** o **static/js/<app>** dependiendo de como sea la logica que estemos trabajando, se recomienda siempre el uso de la segunda estructura. luego dependiendo de como lo estamos cargando en la vista final es decir el enlace src de la etiqueta script en la vista si este es compilado(minificado) o el script en si, si es compilado que es lo recomendable este debe estar indicado en el atributo **settings.compile** del archivo **app.js** de tu aplicacion, esto le indicara a webpack que es un archivo a compilar y como sera el nombre de este.

segun lo anterior imaginemos que tengo una aplicacion de jsee llamada myapp en la cual esto creando componentes. El ejemplo seria el siguiente:

.. code-block:: html
	//apps/myapp/static/components/myapp/app.vue
	<template>
	  <movie-card
	     title="Regreso al Futuro"
	     image="http://es.web.img3.acsta.net/pictures/14/04/03/13/45/034916.jpg">
	  </movie-card>
	</template>
	<script>
	export default {
		name: 'MovieCard',
		props: ['image', 'title'],
	    template: `
	   <div>
	         <img width="100" v-bind:src="image" v-bind:alt="title"/>
	         <h2>{{ title }}</h2>  
	  </div>
	  `,
	  })
	</script>

.. code-block:: javascript
	//apps/myapp/static/js/main.js

	//cargamos nuestra herramienta de integración
	var VueApp=require("Public/js/lib/vueapps.js").default

	//crea una intancia de un manejador e indica la apliacion actual
	var vueapp=new VueApp("myapp")

	//importamos el componente
	var MovieCard=vueapp.from_component("myapp","myapp/app.vue").default

	new Vue({
	  el: '#app',
	  data: {
	    movies: [
	      { title: 'Regreso al Futuro', image: 'http://es.web.img3.acsta.net/pictures/14/04/03/13/45/034916.jpg' },
	      { title: 'Matrix', image: 'http://t0.gstatic.com/images?q=tbn:ANd9GcQq3pIz-aKgkmYX1dJ-EL-AlHSPcOO7wdqRIJ5gJy9qNinXpmle' },
	      { title: 'Interestellar', image: 'http://t1.gstatic.com/images?q=tbn:ANd9GcRf61mker2o4KH3CbVE7Zw5B1-VogMH8LfZHEaq3UdCMLxARZAB' }
	    ],
		components: {"movie-card":MovieCard}
	  }
  })

.. code-block:: javascript
	//apps/myapp/app.js

	const AppConfig=require("../../lib/apps").AppConfig
	class App extends AppConfig{
		constructor(name,app){
			super(name,app)
			this.settings.compile={
				"main":"static/js/main.js",
				}
			
		}
			
		
	}
	module.exports=App








El trabajo de componentes tiene 2 modos:

	* Componente de la propia aplicacion o de uso como libreria
	* Componentes distribuidos por rutas


Componente de la propia aplicacion o de uso como libreria
**********************************************************
Esto es basicamente el ejemplo anterior y el truco esta en el uso de el metodo from_component de VueApp este metodo nos importara el archivo js que se encuentre en **static/components/** de la aplicacion que le hayamos especificado como primer parametro, el segundo parametro es la continuacion de la ruta  **static/components/**.

**Nota**: por alguna razon que aun no se conoce cuando hacemos **require(...)** debemos acceder al atributo *default* para poder acceder a nuestro componente ejemplo **require("<componente>").default** 


Componentes distribuidos por rutas
**************************************

esto nos permitira tener una estructura bien formada para emplear los vue_hooks los cuales son como una especie de fucionador de componentes entre diferentes aplicaciones, para esto primero creamos un archivo llamado **routes.vue** en **static/components/** este archivo deberia solo exportar un array con las rutas de los componentes importados correspondientes a la aplicacion actual 

.. code-block:: javascript
 
	//settings.js

	...
	vue_hooks:{
		"app1":[["app2","app2/routes.vue"],//app, nombre del archivo de montaje como main.js
					],//son los ganchos de la app que aplicaciones cargan cosas como por ejemplo rutas de vue a la aplicacion hook
		},
	...

.. code-block:: html

	<script >
	// apps/app2/static/components/app2/routes.vue

	exports.routes=[
	{path:"/servicios",name:"servicios",component:()=>import("./Servicios.vue")}
	]
	</script>

.. code-block:: html

	<script >
	// apps/app1/static/components/app1/app.vue
	import Vue from "vue"
	import Router from "vue-router"
	Vue.use(Router)

	var VueApp=require("Public/js/lib/vueapps.js").default

	var vueapp=new VueApp("app1")//crea una intancia de un manejador de aplicaciones de vue

	var routes=[
	{path:"/",name:"home",component:()=>import("./Home.vue")},
	{path:"/about",name:"about",component:()=>import("./About.vue")}
	]

	//carga las rutas desdes las otras aplicaciones
	routes=routes.concat(vueapp.init_routes())

	</script>
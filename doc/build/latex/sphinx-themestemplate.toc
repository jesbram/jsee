\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Primeros pasos}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Instalación}{1}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Iniciar nuevo proyecto}{1}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Trabajar con un proyecto que ya usa JSee}{1}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Iniciando el servidor}{1}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}Estructura de trabajo}{2}{subsection.1.1.4}%
\contentsline {subsection}{\numberline {1.1.5}Aplicaciones incluidas en JSee}{2}{subsection.1.1.5}%
\contentsline {chapter}{\numberline {2}Aplicaciones}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Crear una aplicación}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Programacion de aplicacion}{3}{section.2.2}%
\contentsline {chapter}{\numberline {3}Sistema de documentacion}{5}{chapter.3}%
\contentsline {chapter}{\numberline {4}Rutas de vistas}{7}{chapter.4}%
\contentsline {chapter}{\numberline {5}Rutas API}{9}{chapter.5}%
\contentsline {chapter}{\numberline {6}Middlewares}{11}{chapter.6}%
\contentsline {chapter}{\numberline {7}Modelos}{13}{chapter.7}%
\contentsline {chapter}{\numberline {8}Extención de modelos}{15}{chapter.8}%
\contentsline {section}{\numberline {8.1}Extención de campos}{15}{section.8.1}%
\contentsline {section}{\numberline {8.2}Extención de metodos}{15}{section.8.2}%
\contentsline {chapter}{\numberline {9}Filtros}{17}{chapter.9}%
\contentsline {chapter}{\numberline {10}Señales}{19}{chapter.10}%
\contentsline {section}{\numberline {10.1}VuApp}{19}{section.10.1}%
\contentsline {chapter}{\numberline {11}Sistema de plantillas}{21}{chapter.11}%
\contentsline {chapter}{\numberline {12}Vistas}{23}{chapter.12}%
\contentsline {section}{\numberline {12.1}ResourcesView}{23}{section.12.1}%
\contentsline {chapter}{\numberline {13}Motor de formularios}{25}{chapter.13}%
\contentsline {section}{\numberline {13.1}Creando formularios}{25}{section.13.1}%
\contentsline {section}{\numberline {13.2}Widgets}{26}{section.13.2}%
\contentsline {section}{\numberline {13.3}Componentes}{26}{section.13.3}%
\contentsline {chapter}{\numberline {14}Componentes}{27}{chapter.14}%
\contentsline {section}{\numberline {14.1}Como crear/integrar un componente}{27}{section.14.1}%
\contentsline {subsection}{\numberline {14.1.1}Componente de la propia aplicacion o de uso como libreria}{27}{subsection.14.1.1}%
\contentsline {subsection}{\numberline {14.1.2}Componentes distribuidos por rutas}{28}{subsection.14.1.2}%
\contentsline {chapter}{\numberline {15}Lista de componentes}{29}{chapter.15}%
\contentsline {chapter}{\numberline {16}Widgets}{31}{chapter.16}%
\contentsline {section}{\numberline {16.1}Como crear un widget}{31}{section.16.1}%
\contentsline {chapter}{\numberline {17}Lista de widgets}{33}{chapter.17}%
\contentsline {chapter}{\numberline {18}Permisos y grupos}{35}{chapter.18}%
\contentsline {section}{\numberline {18.1}Grupos}{35}{section.18.1}%
\contentsline {chapter}{\numberline {19}Aplicación Dashboard}{37}{chapter.19}%
\contentsline {section}{\numberline {19.1}admin}{37}{section.19.1}%
\contentsline {chapter}{\numberline {20}Indices and tables}{39}{chapter.20}%

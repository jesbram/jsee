
Rutas API
==========================================

Esta clases de rutas tiene la particularidad de que empienzan con el slug base /json. esto quiere decir que si tenemos una ruta /prueba en una aplicacion llamada myapp entonces mi ruta final seria algo como /json/myapp/prueba

una cosa importante es saber que las rutas API se diferencia de las rutas normales tambien en el que la respuesta que esta devuelva debe estar en formato json, es decir si en el caso de las rutas normales estas usan el metodo res.render aqui se utiliza el metodo res.json

para mas informacion sobre la creacion de rutas vea :ref: `Rutas de vistas`
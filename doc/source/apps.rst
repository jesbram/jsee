
Aplicaciones 
==========================================

Básicamente es una clase que ofrece utilidades para el manejo de los recursos que necesitan las aplicaciones y también permite gestionar las dependencias que se generen a través de estas, es decir que una aplicación para funcionar necesita que otra esté activada.

Crear una aplicación 
--------------------------

Para crear una aplicacion podemos compiar la carpeta lib/snippets/myapp dentro de apps y renombrar myapp por el nombre que tendra nuestra aplicacion

Programacion de aplicacion
-------------------------------

Las aplicaciones de JSee usan como nucleo el archivo app.js este contiene una clase llamada App la cual hereda todas las funcionalidades de lib/apps.js a modo que que podemos crear metodos personalizados para la instancia de la aplicacion la cual es nuestra navaja suiza a la hora de desarrollar.

la instancia de la aplicacion se pasa de forma automatica a distintas partes como lo son:

* apis 
* routes
* sockets
* middlewares

algunos de los metodos que usaremos son:

get(): Con este metodo optenemos el nombre de la aplicacion actual que esta siendo rastreada

set(name): Con este metodo establecemos por el nombre a la aplicacion actual a ser rastreada. El rastreo de las aplicaciones se hace con el fin de detectar las dependencias que estas puedan tener unas de otras

get_models_from_app(app): Obtenemos una lista de modelos que tenga una aplicacion

get_model_from_app(app,model): Obtenemos el modelo de una aplicacion, este tipo de metodos los vamos a usar mucho cuando queramos traernos cosas desde otras aplicaciones para asi poderlas rastrear. Otra cosa a tener en cuenta el valor del parametro model debe ser el que se le coloca al modelo en el metodo mongoose.model despues del punto, recordar que los nombres de los modelos tienen el siguiente formato "appname.ModelName" ejemplo: mongoose.model('dashboard.Option', schema)

get_all_models: devuelve todo la lista de modelos de todas las aplicaciones

get_models: devuelve una lista de modelos de la aplicacion 

get_app(app): Devuelve la instancia de otra aplicacion

get_models_registered(): Devuelve los modelos registrados en el administrador de modelos que pertenecen a la aplicacion 


get_all_models_registered(): Devuelve todos los modelos registrados en el administrador de modelos en todas las aplicaciones

get_model(name): Devuelve el modelo por el nombre del archivo sin el .js de la aplacion actual

get_forms(): Devuelve la importacion del archivo forms.js de la aplicacion 

load_library(app,source): Devuelve la importacion de un archivo desde una app esto es util por si tenemos un archivo que queramos traer desde otro directorio que no sea los que manejamos opor defecto dentro de la app

register(model): Este metodo es usado en el archivo admin.js y sirve para registar los modelos en el administrador de modelos
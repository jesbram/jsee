Sistema de documentacion
================================

Con la finalidad de que cada aplicacion/modulo del sistema a desarrollar se mantenga cierta independencia de otras funcionalidades, cada aplicacion debera otorgar a las demas alguna especie de API con la cual estas puedan interactuar, comunicarse y expandirse, esto da paso que que cada aplicacion debe tener una documentacion desde la cual los demas miembros de un equipo de trabajo puedan consultar y asi hacer uso de este tipo de API. como ya hemos descrito antes, igualmente dejamos por aqui la lista de API que podemos usar.

* Señales. Ver :ref: `Señales` 
* Filtros.  Ver :ref: `Filtros` 
* Campos extendidos. Ver :ref: `Extención de campos de modelos` 
* Plantillas. Ver :ref: `Señales` 
* API rest. Ver :ref: `Rutas API` 

Como veremos el sistema de documentacion que usamos es sphinx que es un motor de documentacion muy popular en en el ambiente de Python y ofrece multiples ventajas a la ora de utilizar su marcado, El lenguaje de marcado es RestructureText este es muy parecido al MarkDown habitual en los documentos README de los repositorios git. 

Para generar la documentacion de tu aplicacion simplemente basta con crear una carpeta llamada doc y dentro colocar un archivo llamado index.rst una ves creado podemos colocarle un titulo y una descripcion tal que asi

.. code-block::text
	Documentacion de mi aplicacion
	=================================

	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

ya en este punto estaremos siguiendo las reglas de estilo del lenguaje de marcado de RestructureText y finalmente corremos nuestro asistente de  frameworks Cobra de la siguiente manera

.. code-block::text
	npm run cobra 

y marcamos la opcion 4) Construir documentación y finalmente ejecutando nuestro servidor 

.. code-block::text
	npm run dev

podemos ir a la direccion http://localhost:3000/wiki o haciendo click en el enlace ver documentacion de nuestra pagina de bienvenida del framework
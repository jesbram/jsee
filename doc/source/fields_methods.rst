
Extención de modelos 
==========================================

La extension de modelos es la forma por la cual se le puede dar nuevas estructuras a los modelos de otras apps sin tener que modificar directamente el codigo de estas, esto es muy util cuando estamos trabajando con metodos o campos que pertenecen a otros modelos que usa el sistema pero los metodos o campos en cuestion hace referencia es a la aplicacion actual en la que estamos trabajando. La extencion de modelos son de 2 tipos:
	* Extencion de campos 
	* Extencion de metodos

Extención de campos
-------------------------------
Esta documentación te explicara como funciona la extención de campos de modelos 
esto con la intencion de no modificar directamente el codigo de estas aplicaciones
y asi evitar una posible inestabilidad al modificar codigo estable.



Podemos exteneder los campos de un modelo creando un archivo llamado fields.js en nuestra aplicacion, este archivo pertenece a los archivos de carga automatica con lo cual solo nos bastara con crear este archivo y colocarle un codigo como el siguiente:

.. code-block:: javascript
	:linenos:

	const mongoose = require('mongoose')

	module.exports={
		"auth/user":{"inclusive_access":{
			"objects":[{
	      type: mongoose.Schema.Types.ObjectId,
	      ref: "objects.Object"
	    }]
		}}
	}

Aqui "auth/user" hace referencia a que se esta expandiendo los campos de el modelo user de la aplicacion auth, como se puede ver en este caso se estan agregando al campo inclusive_access un subcampo llamado objects


Extención de metodos
-------------------------------

Esta es similar a la extencion de campos solo que al modelo le pasaremos una funcion cuyo argumento recibira el schema del modelo en el cual crearemos los metodos de la siguiente manera.

.. code-block:: javascript 
	:linenos:
	
	module.exports={
	"auth/user":function(schema){
		schema.statics.prueba=function(){
			console.log("uuuuuuuu")
		}
	}
}
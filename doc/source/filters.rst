
Filtros
==========================================

Los filtros son puntos de anclaje para la modificacion de informacion de forma extendible, es decir que podriamos modifcar en las plantillas de las vistas donde esta tenga un filtro la informacion que alli se motraria como puede ser un titulo, un texto de descripcion, etc.

Para hacer uso de esto creamos un archivo llamado filters.js  dentro de nuestra aplicacion al nivel del archivo app.js,  dentro de este archivo iremos enlazando funciones al nombre de hooks que luego podemos usar en diferentes partes de las plantillas que usemos

.. code-block:: javascript
	:linenos:
	module.exports = function (server,app) {
	

		app.add_filter("login_footer",function(html,data){
			if (!html){//Esto es porque html puede ser null y nosotros queremos concatenar
			 html=""
			}
			return html+"<script src='/js/dist/users-login.js'></script>"
		})
	}


Luego de esto el modo de uso es muy simple, donde queramos ejecutar, por lo general una plantilla html usamos el metodo `apply_filters` all cual le pasamos el nombre del filtro que queramos llamar, un primer valor o string vacio y luego la demas data que manejara el filtro


.. code-block:: html
	:linenos:

	<div>
	<%-apply_filters("login_footer","",{})%>
	</div>
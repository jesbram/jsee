
Motor de formularios
==========================================

Esta utilidad se refiere a que podemos tener un sistema que integre fácilmente componentes que ya esten desarrollados con unas pocas lineas de codigo, como pueden ser componentes de vue, element.io, bootstrap, etc. Además de la generación de campos de forma dinámica desde los modelos

Creando formularios
------------------------------------------------

En JSee existen 2 tipos de formularios estan los ModelForm y lo Form ambos son clases que se importan desde lib/forms.js. Los ModelForm son formularios que se crean a partir de los modelos, es decir que tenemos la facilidad de extraer los nombres de los campos de los modelos que estamos usando para el formulario, esto tambien es util ya que nos permitira tener metodos de validación y widget predeterminados para cada campo.

Para hacer uso de esto se recomienda crear un archivo llamado forms.js al nivel de el archivo app.js de nuestra aplicacion, alli crearmos nuestras clases de formularios que posteriormente incorporaremos en nuestras vistas.

de momento este tutorial esta pensado en vistas basadas en ejs aunque se expera proximamente este tipo de formulario tenga una implenteacion directa tambien en vue.

un formulario se crea de la siguiente manera:

.. code-block:: javascript
	:linenos:

	const path = require('path');
	const appRoot = require('app-root-path');
	var Apps=require(path.join(appRoot.path,"lib/apps.js"))
	var forms=require(path.join(appRoot.path,"lib/forms.js"))
	var User=Apps.get_app("auth").get_model("user")

	class PruebaForm extends forms.ModelForm{
		constructor(instance,data){
			super(instance,data,{
				model:User,
				fields:"__all__",
				widgets:{"password":new forms.TextInput({"type":"password"})}
			})
			
		}
	}
	exports.PruebaForm=PruebaForm

En este ejemplo estamos creando un formulario tipo ModelForm por lo cual le esatamos pasadon al constructor el modelo User.

parametros del contructor:

* instance: se refiere al la instancia del modelo, es decir cuando hacemos algo como Model.findById, esto quiere decir que cuando este parametro se pasa el formulario estaria editando un registro previamente creado

* data: se regiere a los datos del req.body que son pasados despues de haber hecho un submit

* meta: este tercer parametro es un json que puede contender diversas cosas entre ellas:
	
	+ model: El modelo que esta siendo usado, en caso de los ModelForm
	+ fields: Lista de campos que se usaran en el formulario "__all__" indica que todos son visibles
	+ widgets: este parametro lo usaremos cuando queramos sustituir algun widget por defecto del formulario por otro distinto 


Podemos ver que en este caso al widget del campo password le estamos cambiando el attributo type por password, por defecto los campos de textos tienen el widget TextInput

Widgets
------------

Son el Codigo HTML de la cual se enganchara por ejemplo los componentes de Vue que queramos usar

Para ver la lista de widgets disponibles en JSee haga click en el siguiente enlace: :ref: `Lista de Widgets`

ver mas:ref: `Widgets`

Componentes
------------

Son ya como su nombre lo indica componentes en este caso principalmente de vue que tomaran los datos pasados a los widgets para estos renderizarse y poder ser usados en el formulario 

Para ver la lista de componentes disponibles en JSee haga click en el siguiente enlace: :ref: `Lista de Componentes`
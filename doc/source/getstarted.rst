
Primeros pasos
==========================================

Prerequisitos:
 * Nodejs
 * mongodb

Instalación
***********************

Iniciar nuevo proyecto
--------------------------

Para instalar el framework se debera realizar los siguientes pasos:

* clonar el repositorio desde el siguiente enlace https://gitlab.com/jesbram/jsee.git


Trabajar con un proyecto que ya usa JSee
-------------------------------------------
Para esto debera usar los siguientes pasos:

* clonar su repositiorio 
* ejecutar git submodules init y git submodules update para clonar los submodulos, esto es importante para habilitar cobra en el sistema
* luego ejecutar npm install o npm ci

Iniciando el servidor
------------------------------

para correr el servidor ejecute npm run dev esto iniciar el un servidor http de express en el puerto 3000, esto puede cambiarse modificando el archivo config.json

Para crear un usuario debera usar el siguiente enlace 

https://localhost:3000/register 

Nota: los usuarios creados son usuarios sin ninguna permisologia para asignarle alguna permisologias puede consultar el siguiente enlace: :ref:`Permisos y Grupos`


Estructura de trabajo
------------------------------

JSee centra su estructura de trabajo principalmente en la carpeta apps/ aqui se colocaran los modulos/funcionalidades que el sistema que se este desarrollando tendran, a estas funcionalidades se les da el nombre de aplicaciones


Estructura de aplicación
	Para agilizar, modularizar y mantener el codigo limpio las aplicaciones tienen la siguiente estructura de directorios

	* apis: Todas las rutas que devuelven un json
		+ index.js: archivo principal donde se crean las rutas. vea tambien :ref:`Crear rutas apis`
	* routes: Todas las rutas que devuelven una vista, basicamente .ejs
		+ index.js archivo principal donde se crean las rutas. :ref:`Crear rutas de vistas`
	* sockets: Donde se desarrolle todo lo que tenga que ver con socket de la aplicación. vea tambien :ref:`Crear sockets`

	* models: Donde se encuentran los modelos de la aplicación. vea tambien :ref:`Crear modelos`
	* static: Donde se colocan todos los archivos de la aplicacion, js, css, imgs,fonts, etc
	* components: básicamente los componentes de vue que use la aplicación o que otra aplicación pueda tomar de esta
	* views: todas las vistas .ejs y .vue  que use la aplicacion, aqui los .vue se cargan a partir de un .ejs
	* app.js: donde se heredan todas las funcionalidades de manejo de aplicaciones y se estable si se quiere configuraciones de la propia aplicacion. Vea tambien :ref:`Aplicaciones`
	* middlewares.js (opcional, carga semiautomatica):middlewares que se quieran colocar para la aplicacion. Vea tambien :ref:`Formularios`
	* forms.js (opcional): donde se coloca los formularios que se van a crear para la aplicación, como por ejemplo los que se generen apartir de los modelos, para esto hereda del motor de formularios. Vea tambien :ref:`Formularios`
	* signals.js(opcional, carga automatica): son donde se colocan las funciones que vas a utilizar los hooks que se coloque por la aplicación. Vea tambien :ref:`Señales`
	* fields.js (opcional, carga automatica): donde se heredan todas las funcionalidades de manejo de aplicaciones y se estable si se quiere configuraciones de la propia aplicacion. Vea tambien :ref:`Expanción de campos`
	* resourcesviews.js: donde se heredan todas las funcionalidades de manejo de aplicaciones y se estable si se quiere configuraciones de la propia aplicacion. Vea tambien :ref:`Aplicaciones`
	* admin.js: donde se heredan todas las funcionalidades de manejo de aplicaciones y se estable si se quiere configuraciones de la propia aplicacion. Vea tambien :ref:`Registrar modelos en el administrador de modelos`

Aplicaciones incluidas en JSee
--------------------------------
	* auth: Todo el sistema de gestión de usuarios que por defecto maneja el framework, aquí es donde se está trabajando lo referentes a la permisología y roles.:ref:`Aplicación Auth`
	* dashboard: En general esta aplicación que viene por defecto en el framework se iba a usar para ofrecer código entre aplicaciones y de ejemplo de cosas del framework. pero actualmente también se está desarrollando como un sistema para manejar datos del los modelos a modo también de ejemplo de lo que se puede hacer con las utilidades del framework. para ver mas vea :ref:`Aplicación Dashboard`


.. jsee framework documentation master file, created by
   sphinx-quickstart on Wed Aug  5 12:18:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenidos a la documentación del Framework JSee
==========================================

.. toctree::
   getstarted.rst
   apps.rst
   documentation.rst
   routes.rst
   apis.rst
   middlewares.rst
   models.rst
   fields_methods.rst
   filters.rst
   signals.rst
   templates.rst
   views.rst
   forms.rst
   components.rst
   list_components.rst
   widgets.rst
   list_widgets.rst
   permissions_groups.rst

   ../../apps/dashboard/doc/index
   :maxdepth: 2
   :caption: Contents:
   




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

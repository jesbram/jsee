
Middlewares
==========================================

Son middlewares que carga el core por defecto, nada especial. Tambien hay middlewares que son propios de las aplicaciones, para esto se crea en la aplicacion al nivel del archivo app.js un archivo de nombre middlewares 

este contrentra la siguiente estructura

.. code-blocks:: javascript
	exports.middlewares=function (server,apps) {
	}

Esto junto con colocar el middleware en settings.js de la siguiente forma habilitara el uso del middleware en el sistema

.. code-blocks:: javascript
	middlewares:[
		"auth/middlewares"
		],
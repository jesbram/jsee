Permisos y grupos
===========================

El sistema de permisos de JSee hace uso de 3 campos dependiendo de como sea el diseño que se quiera implementar en tu sistema.

campos:

* inclusive_access: Este campo es usado para que solo los items de los subcampos en el tiene el permiso para acceder a algun lado del sistema  
* exclusive_access:Este campo es usado para que solo los items de los subcampos en el no tiene el permiso para acceder a algun lado del sistema  
* permissions: Este campo se refiere mas a los permisos en si osea las capacidades que tiene un usuario al manejar determinada parte del sistema, como por ejemplo: ver, editar, modificar, crear, etc

Grupos
----------------------

Son basicamente la agrupación de un conjunto de permisos siendo asi mas facil asignarselos a algun usuario por lote en lugar de uno por uno. El uso mas habiltual para Los grupos de permisos es el de crear roles



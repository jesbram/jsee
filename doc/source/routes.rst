
Rutas de vistas 
==========================================

Las rutas se crean de dentro del archivo index.js en la carpeta routes, el codigo del archivo tiene la siguiente estructura.

.. code-block:: javascript

	const passport = require('passport');
	const express = require('express');
	const path = require('path');
	const appRoot = require('app-root-path');
	module.exports = function (server,app,router) {
		var router = express.Router();
		router.get("/test1",
			async (req, res) => {
				
				res.render("blank.ejs",{app:app.name})
			})		
		return router
	}

la linea module.exports = function (server,app,router)  nos dice que se exportar una funcion la cual recive 3 parametros

* server: es la instancia de express 
* app: es la instancia de la aplicacion actual, es decir la instancia que deriva de la clase en el archivo app.js de la aplicacion
* router: en caso de este no ser el archivo index.js sino un archivo diferentes este parametro debera ser diferente a null ya que se le estar pasando como parametro el router previamente declarado en index.js
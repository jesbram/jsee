
Señales
==========================================

Esto es una copia del sistema de hooks action que usar wordpress, la idea es que se puedan colocar hook en partes del codigo desde los cuales ampliar el codigo de esa aplicación sin necesidad de editar la propia aplicación, esto permite a otros programadores que necesiten integrar una funcionalidad en otra aplicación la cual estos no estan acargo poder hacerlo siguiendo solo la lista de hooks que el responsable de la aplicación le proporcione.

Para esto creamos un archivo llamado signals.js en nuestra apliacion al nivel de app.js
con la siguiente estructura 

.. code-block:: javascript
	:linenos:
	
	//signals.js

	module.exports = function (server,app) {

		app.add_signal("prueba",function(data){
			//definimos nuesta funcion
		})
	}


Para ejecutarlo simplemente la ubicamos en la parte de un codigo donde deseemos que esta se active y usamos ´do_signal´ le pasamos el nombre de la señal y los datos que va a utilizar. dependiendo de donde estemos si poseemos la instancia de alguna aplicacion o del server esta tendra el metodo do_signal

.. code-block:: javascript
	:linenos:

	module.exports = function (server,app,router) {

		route.get("/prueba",function(req,res){
			//un ejemplo de ejecutar una señal dentro de una ruta 
			app.do_signal("prueba",{})

		})

	}

VuApp
.............

como ya sabemos las señales que vimos anteriorente son señales de backend, pero con el uso de VueApp podemos crear y usar signals en el frontend, esto sera muy util a la hora de ejecutar eventos que transmitan información a distintos componentes, la diferencia aqui es que el archivo signals.js no se carga de forma automatica como lo hace el del backend, ademas de como sabemos al ser de frontend este debe ubicarse dentro de la carpeta static ya alli se le da la ubicacion que queramos, recomendado usar el nombre de alguna app seguido de js por ejemplo static/dashboard/js.

.. code-block:: javascript
	:linenos:

	//signals.js
	var VueApp=require("Public/js/lib/vueapps.js").default
	var vueapp=new VueApp("dashboard")

	vuapp.add_signal("prueba",function(data){
		console.log(data["message"])
	})


.. code-block:: javascript
	:linenos:

	//main.js
	import * as Signals from "./signals.js"
	var VueApp=require("Public/js/lib/vueapps.js").default
	var vueapp=new VueApp("dashboard")

	//ejemplo de ejecucion de una señal
	vuapp.do_signal("prueba",{"message":"hola mundo"})


Sistema de plantillas
=====================

El sistema de plantillas que usa es ejs, las vistas se crean en la carpeta views de cada aplicacion y todas estas funcionan en su conjunto como si fuesen una sola carpeta por lo cual se recomienda crear una carpeta dentro de views la cual tenga el nombre de la aplicacion ya de esta manera estas estaran mas identificadas con dicha aplicacion. La idea que todas las carpetas views se comporten como una misma carpeta es que le permite al desarrollador sin mucho esfuerzo incorporar en su aplicacion plantillas que puedan ser de otra aplicacion.

Vistas
==========================================

ResourcesView
--------------------------
Esta utilidad es una copia de las vistas de recursos de Laravel, para verlo mas claro es una clase de la cual se hereda todas las vistas que incluye un CRUD básico insertar, listar, ver, eliminar,editar. con el fin de agilizar cualquier nueva funcionalidad que se le quiera incorporar al sistema. ejemplo:

.. code-block:: javascript
	:linenos:

	//tenemos nuestro apps/app/routes/index.js
	module.exports = function (server,app,router) {
	//importo mi clase creada 
	// app se remplaza por el nombre de la aplicación, 
	//aunque mas adelante puedo hacer un cargador para no colocar la ruta completa 
	//obviamente tendria que crear el archivo views.js o con el nombre que sea,
	// esto es similar a forms.js
	var views=require(path.join(appRoot.path,"apps/app/views.js"))
	router.get('/ruta-cualquiera',
   	 (req, res) => {
   		 res.render(“plantilla.ejs”,{})
   	 })
	//añado mi resourceview el cual e creado previamente
	new views.PruebaView(“/slug-base”,router)
	}

.. code-block:: javascript
	:linenos:
	//tenemos nuestro apps/app/views.js que hemos creado
	var ResourceView=require(path.join(appRoot.path,"lib/ResourceView.js"))
	class PruebaView extends ResourceView{
		constructor(base_slug,router,app){
			super(base_slug,router,app,{
		   		 model:function(self,req){
		   			 return self.app.get_model_from_app(req.params.app,req.params.model)
		   		 }
		   	 })
		}
	}

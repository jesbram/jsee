Widgets
============

Los widgets son clases la cuales poseen un metod render el que es usando para devolver el html con el que se representara un determinado campo, este html muchas veces sera usado como el template que usan los componentes de Vue para construirse. Los widgets se importan en lib/forms.js

Como crear un widget
--------------------------

Si nosotros lo que queremos es hacer un widgets personalizado podemos hacerlo de la siguiente manera

crearemos un archivo con el nombre widgets.js alli colocaremos todos nuestros widgets personalizados que vayamos creando 

La estructura base para crear un widget es la siguiente

.. code-block::javascript

	class MyWidget{
		constructor(attrs){
			this.attrs=attrs
		}
		render(){
			var _attrs=""
			for (var elem in this.attrs){
				_attrs+=elem+"='"+this.attrs[elem]+"' "
			}
			return '<input '+_attrs+'>'
		}
	}

Lo primero sera crear una clase la cual tenga un parametro el cual llamaremos attrs, este parametro sera usado para pasar un json del cual se formaran los atributos que tendra nuestra etiqueta html

Luego definimos el metodo render este metodo devuelve un string el cual debe ser el HTML que sera la representacion de nuestro campo 

const port = 3000
app=require("./lib/server")(port)
const express = require('express');
//rutas 
var router = express.Router();

app.use("/",app.load_router("auth"))
app.use("/dashboard",app.load_router("dashboard"))

app.auto_routes()
app.auto_apis()

// 404
app.use('*', function (req, res) {
  res.error(404, "Page Not Found", true)
})
app.use(function (err, req, res, next) {
	console.log(err)
  res.error(500, "Internal Error", true)
})



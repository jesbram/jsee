const path = require('path');
const appRoot = require('app-root-path');

const { EvalSourceMapDevToolPlugin } = require('webpack');

var Model=require(path.join(appRoot.path,"lib/model.js"));
var forms=require(path.join(appRoot.path,"lib/forms.js"));

const fixed_url = url => {
	let fix_url = url.split('?')
	if(fix_url[0].slice(-1) === '/'){
		let url = fix_url[0].slice(0,-1)
		return url
	} else{
		return fix_url[0]
	}

}

async function generate_data(self,req,exclude){
	var fields=[]
	var rows=[]
	if (typeof(self.meta.model)==="function"){
		var model=self.meta.model(self,req)
		fields=Model.get_fields(model)
	}
	else{
		fields=Model.get_fields(self.meta.model)
	}

	for (var elem of exclude){
		var i=fields.indexOf(elem)
		fields.splice(i, 1)
	}


	// test = new op({name: "test_0", value: "value_0"});
	// test.save()
	
	//model=self.app.get_model_from_app(req.params.app,req.params.model)
	model=self.meta.model(self,req)


	data = []
	// ({}, function(err, docs)
	var search=[]
	if (req.query.q){
		for (var elem of fields){
			d={}
			if (model.schema.tree[elem].hasOwnProperty("type") && model.schema.tree[elem]["type"]==String){
				d[elem]=req.query.q
				search.push(d)
			}
			else if (model.schema.tree[elem]==String){
				d[elem]=req.query.q
				search.push(d)
			}
			
			
		}
		
		await model.find({$or:search},fields.join(" "), function(err, docs){
			data = docs
		}).exec();

	}
	else{
		
		await model.find({}, fields.join(" "),function(err, docs){
			data = docs
	
			
		}).exec();
		

	}
	

	let num_page = req.query.page === undefined ? 1 : parseInt(req.query.page)
	let per_page = 5
	let to_page = (num_page * per_page)
	let from_page = (num_page * per_page) - per_page
	let items_per_page = data.slice(from_page, to_page)
	let total_pages = Math.ceil(data.length / per_page)

	rows = items_per_page

	const addNumPage = () => {
		num_page++
	}

	const substracNumPage = () => {
		num_page--
	}

	let url = fixed_url(req.originalUrl)
	// model=self.app.get_model_from_app(req.params.app,req.params.model)
	// num_page = parseInt(req.query.page)
	// skip_page = (num_page-1)*20
	// model.count().then(function ( count ){
	// 	num_pages = parseInt((count/20)+1);
	// });
	/*
	rows=model.find({ is_active: true },{username:1, personal_info:1})
	.skip(skip_page)
	.limit(20)
	.lean()
	*/

	return {fields:fields,rows:rows,search:req.query.q?req.query.q:"", total_pages, num_page, addNumPage, substracNumPage, url }
}

async function get_user(self, req) {
	var fields=[]
	var row=[]
	if (typeof(self.meta.model)==="function"){
		var model=self.meta.model(self,req)
		fields=Model.get_fields(model)
	}
	else{
		fields=Model.get_fields(self.meta.model)
	}
	
	//model=self.app.get_model_from_app(req.params.app,req.params.model)
	model=self.meta.model(self,req)
	data = []

	await model.find({}, function(err, docs){
		data = docs		
	});
	
	row = data.filter(elem => {
		return(elem._id == req.params.id)
	})

	return {fields, row}
}



class ResourceView{

	constructor(slug,router,app,meta,options){
		var that=this
		that.app=app
		if (meta.middleware==undefined){
			meta.middleware=function(req,res,next){
				return next()
			}
		}
		
		router.get(slug,meta.middleware,function(req,res){that.index(that,req,res)})
		router.get(slug+"/new",meta.middleware,function(req,res){that.create(that,req,res)})
		router.post(slug+"/new",meta.middleware,function(req,res){that.created(that,req,res)})
		router.get(slug+"/:id",meta.middleware,function(req,res){that.show(that,req,res)})
		router.get(slug+"/:id/edit",meta.middleware,function(req,res){that.edit(that,req,res)})
		router.post(slug+"/:id/edit",meta.middleware,function(req,res){that.edited(that,req,res)})
		router.get(slug+"/:id/delete",meta.middleware,function(req,res){that.delete(that,req,res)})
		router.delete(slug+"/:id/delete",meta.middleware,function(req,res){that.deleted(that,req,res)})
		this.meta=meta 
		this.options=options
		if (!this.meta.index_view){
			this.meta.index_view="dashboard/table.ejs"
		}
		if (!this.meta.create_view){
			this.meta.create_view="dashboard/edit.ejs"
		}
		if (!this.meta.show_view){
			this.meta.show_view="dashboard/show.ejs"
		}
		if (!this.meta.delete_view){
			this.meta.delete_view="dashboard/delete.ejs"
		}
		if (!this.meta.new_view){
			this.meta.new_view="dashboard/new.ejs"
		}
		if (!this.meta.edit_view){
			this.meta.edit_view="dashboard/edit.ejs"
		}

		

	}
	actions(self,req){
		return function(id){
			var model=self.meta.model(self,req)
			var url=req.originalUrl
			if (!url.endsWith("/")){
				url+="/"
			}
			var btns=["<a style='font-size:12px;padding:1px' href='"+url+id+"/edit'>Editar</a>",
					  "<a style='font-size:12px;padding:1px;color:red' href='"+url+id+"/delete'>Eliminar</a>"]
			return "<div style='padding:2px'>"+btns.join("")+"</div>"
		}

	}
	async index(self, req, res){
		var fields=this.exclude_fields(self,"index",req,res)
		var data=await generate_data(self,req,fields)
		data["req"]=req
		
		data["actions"]=this.actions(self,req)
		data["format_field"]=this.format_field
		if (!self.custom_index){
			data["title"]=this.meta.title_index?this.meta.title_index:self.meta.model(self,req).modelName	
		}
		await this.filter_data(self,"index",data)
		
		res.render(this.meta.index_view,data)
	}
	async show(self, req, res){
		let data = await get_user(self,req)
		data["req"]=req
		if (!self.custom_create){
			data["title"]=this.meta.title_index?this.meta.title_index:self.meta.model(self,req).modelName
		}
		await this.filter_data(self,"show",data)
		res.render(this.meta.show_view, data)
	}
	async create(self, req, res){
		let data={}
		data["id"]=req.params.id
		data["req"]=req
		data["method"]="POST"
		if (!self.custom_create){
			data["form"]=new this.meta.form(null,null,this.meta.model(self,req))
			await data["form"].build()
			data["title"]=this.meta.title_index?this.meta.title_index:"Nuevo "+self.meta.model(self,req).modelName
		}
		await this.filter_data(self,"create",data)
		
		
		res.render(this.meta.create_view,data)
	}
	async created(self, req, res){
		let data={}
		if (!self.custom_created){
			data["form"]=new this.meta.form(null,req.body,this.meta.model(self,req))
			data["id"]=req.params.id
			data["req"]=req

		}
		
		await this.filter_data(self,"created",data)
		
		await data["form"].save({"files":req.files})

		
		res.redirect("../")
	
	}
	async edit(self, req, res){
		let data={}
		data["id"]=req.params.id
		data["req"]=req
		data["method"]="POST"
		if (!self.custom_edit){
			let instance=await this.meta.model(self,req).findOne({_id:req.params.id})
			
			data["form"]=new this.meta.form(instance,null,this.meta.model(self,req))
			await data["form"].build()
			if (this.meta.model.modelName=="auth.User"){
				data["form"].fields["password"].widget=new forms.PasswordInput({"name":"password"})

			}
			data["title"]=this.meta.title_index?this.meta.title_index:"Editar "+self.meta.model(self,req).modelName
		}
		
		
		await this.filter_data(self,"edit",data)
		data["form"].instance._files=req.files
		res.render(this.meta.edit_view,data)
	}
	async edited(self, req, res){
		let data={}
		data["req"]=req
		if (!self.custom_edited){
			data["instance"]=await this.meta.model(self,req).findOne({_id:req.params.id})	
			data["form"]=new this.meta.form(data["instance"],req.body,this.meta.model(self,req))
			await data["form"].build()		
		}
		
		
		
		await this.filter_data(self,"edited",data)
		
		
		await data["form"].save({"files":req.files})
		await self.app.do_signal("ResourceView_edited",data)


		res.redirect("./edit")
	}
	async delete(self, req, res){
		var data={}
		data["title"]=this.meta.title_index?this.meta.title_index:"Eliminar "+self.meta.model(self,req).modelName+"["+req.params.id+"]"
		data["method"]="DELETE"
		data["id"]=req.params.id
		await this.filter_data(self,"delete",data)
		res.render(this.meta.delete_view,data)
	}
	async deleted(self, req, res){
		let data={}
		data[req]=req
		await this.filter_data(self,"deleted",data)
		await this.meta.model(self,req).deleteOne({_id:req.params.id})
		res.redirect("../")
	}
	async filter_data(self,name,data){

	}
	async exclude_fields(self,name,req,res){
		return []
	}
	async format_field(self,name,value){
		return value
	}
}

exports.ResourceView=ResourceView
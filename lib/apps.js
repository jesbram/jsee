var apps=[]
const appRoot = require('app-root-path');
const path = require('path');
const fs = require("fs");
const settings = require('../settings');
var warnings=[]
var models=[]
var models_registered=[]
var libraries=[]
var app_current=null
var sockets={}
function get_models_from_app(name){
	if (models[name]){

		return models[name]
	}
	else {return []}
	}
function load_model_from_app(app,name,current){
		var _break=false

		for (var warn of warnings){ //verifica que si ya no esta la dependecia registrada

			if (warn["type"]=="register_dependency" 
				&& warn["data"]["app"]==app 
				&& warn["data"]["app_current"]==current
				&& warn["data"]["source"]==path.join(appRoot.path,"apps/"+app+"/models/"+name )) {
				
				_break=true
				break
			}

		}

		if (!_break){
			warnings.push({"type":"register_dependency",
					   "message":"Registrando dependecia",
					   "data":{"app":app,
					   		   "app_current":current,
					   		   "source":path.join(appRoot.path,"apps/"+app+"/models/"+name)}})
		}
		

		return require(path.join(appRoot.path,"apps/"+app+"/models/"+name))
	}
function get_model_from_app(name,model,current){
	if (models[name]){
		for (var elem of models[name]){
			if (elem[0].modelName==name+"."+model){
				return elem[0]
			}
		}
	}else{
		if ( settings.apps.indexOf(name) >-1){
			console.log("Adelantada carga del modelo '"+name+"'")	
			models[name]=[[load_model_from_app(name,model.toLowerCase(),current),null]]
			return models[name][0][0]
		}
		else{
			console.log("La aplicacion '"+name+"' no se encuentra habilitada")	
		}
	}
	
		
	}
function load_library(app,name){
		
		//carga el modulo de vue de cada aplicacion
		if ( settings.apps.indexOf(app) >-1){

		    if (libraries.indexOf(path.join(appRoot.path,"apps/"+app+"/"+name))>-1){
		    	return libraries[path.join(appRoot.path,"apps/"+app+"/"+name)]
		    }
		    else{
		    	libraries[path.join(appRoot.path,"apps/"+app+"/"+name)]=require(path.join(appRoot.path,"apps/"+app+"/"+name))
		    	return libraries[path.join(appRoot.path,"apps/"+app+"/"+name)]

		    }		    	

		      
		}
		else{
			warnings.push({"type":"no_app_actived",
						   "message":"La aplicacion "+app+" no esta activada",
						   "data":{"app":app}})
		}
		  
		    
		  
		
	}
function load_middlewares_from_app(name){
		//esto se carga asi ya que solo queremos poder acceder a los metodos del middleware
		if ( settings.apps.indexOf(name) >-1){
			return require(path.join(appRoot.path,"apps/"+name+"/middlewares"))
		}
	}

function get_all_models(){
		return models
	}

function set(name){
	app_current=name
}
function get(name){
	return app_current
}
function register_socket(app,source,module){
	sockets[app+"/"+source]=module
}
function get_socket_from_app(app,source){
	return sockets[app+"/"+source]

}

function get_app(name){

	for (var elem of apps){
		
		if (elem.name==name){

			return elem
		}
	}
}


class  AppConfig{
	constructor(name,server){
		this.name=name
		this.server=server
		this.path=path.join(appRoot.path,"apps/"+this.name)
		this.root_path=path.join(appRoot.path,"apps/")
		this.settings={
		}
		set(name)
		

	}
	ready(){}
	get(){
		return get()
	}
	set(){
		set(this.name)
	}
	get_settings(){
		return settings
	}
	get_models_from_app(name){
		return get_models_from_app(name)
	}

	get_model_from_app(name,model){
		return get_model_from_app(name,model,this.name)
	}
	get_all_models(){
		return get_all_models()
	}
	get_models(){
		if (models[this.name]){
			return models[this.name]
		}else{
			return []
		}
		
	}
	get_app(name){
		return get_app(name)
	}
	
	
	get_models_registered(){

		if (models_registered[this.name]){
			return models_registered[this.name]
		}
		else{
			return []
		}
		
	}
	
	get_all_models_registered(){

		return models_registered
	}
	get_model(name){
		return require(path.join(appRoot.path,"apps/"+this.name+"/models/"+name+".js"))
	}
	get_forms(){
		if (fs.existsSync(path.join(appRoot.path,"apps/"+this.name+'/forms.js')))
			return require(path.join(appRoot.path,"apps/"+this.name+"/forms.js"))
	}
	load_router(){
		var that=this;
		return require(path.join(appRoot.path,"apps/"+this.name+"/routes/index"))(this.server,that)
	}
	load_apis(){
		var that=this;
		return require(path.join(appRoot.path,"apps/"+this.name+"/apis/index"))(this.server,that)
	}
	register_socket(app,source){

	}
	get_socket_from_app(app,source){
		return get_socket_from_app(app,source)
	}
	get_socket(socket){
		return get_socket_from_app(this.name,socket)		
	}
	load_middlewares(){
		//esto se carga asi ya que solo queremos poder acceder a los metodos del middleware
		
		return require(path.join(appRoot.path,"apps/"+this.name+"/middlewares"))
	}
	load_middlewares_from_app(name){
		//esto se carga asi ya que solo queremos poder acceder a los metodos del middleware
		return load_middlewares_from_app(name)
		
	}
	load_source(source){
		return require(path.join(appRoot.path,"apps/"+this.name+"/"+source))	
	}
	load_library(app,source){
		return load_library(app,source)
	}
	
	/*
	*Este metodo sera usado para registrar las dependencias al cargar el model
	*/
	load_model_from_app(app,name){
		return load_model_from_app(app,name,this.name)
	}

	register(model,form){
		/*
		Registra el modelo para dashboard y si tiene un formulario de administracion
		crea el widget para el modelo
		*/
		var _break=false

		if (models_registered[this.name]){

			for (var elem of models_registered[this.name]){
				if (elem[0].modelName==model.modelName){
					_break=true
					break
				}
			}
			if (!_break){
				models_registered[this.name].push([model,form])

			}
		}
		else{
			
			models_registered[this.name]=[[model,form]]			
		}
		
		

	}

	





	report_dependecies(){
		//envia la lista de dependecias encontradas (apps) que necesitan estar activadas para que la applicacion actual 
		//pueda funcionar correctamente, de modo que al saber esto el sistema pueda activar todas las dependencias cuando
		//cuando necesite activar esta
		
	}

}
AppConfig.get_models_from_app=get_models_from_app
AppConfig.get_model_from_app=get_model_from_app
AppConfig.get_all_models=get_all_models
AppConfig.load_library=load_library
AppConfig.set=set
AppConfig.get=get

exports.AppConfig = AppConfig

exports.get_app=get_app

exports.load_model=function(name,model){
	return load_app(name).load_model(model)
}
exports.add_app=function(app){
	apps.push(app)
}
exports.get_apps=function(){
	return apps
}

exports.register_model=function(name,model){
	var _break=false
	
	if (models[name]){

		for (var elem of models[name]){
			if (elem[0].modelName==model.modelName){
				_break=true
				break
			}
		}
		if (!_break){
			models[name].push([model])

		}
	}
	else{
		models[name]=[[model]]			
	}
}

exports.get_models_from_app=get_models_from_app
exports.get_model_from_app=get_model_from_app
exports.apps=apps
exports.set=set
exports.get=get
exports.models=models
exports.register_socket=register_socket
exports.get_socket_from_app=get_socket_from_app
exports.get_all_models=get_all_models
exports.load_library=load_library
exports.load_middlewares_from_app=load_middlewares_from_app
exports.get_app=get_app
exports.get_settings=function(){
		return settings
	}
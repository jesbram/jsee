var filters={}
var historical=[]
var ubications=[]
function add_filter(name,fn,priority,file){

	if (priority==undefined){
		priority=10
	}

	if (!filters.hasOwnProperty(name)){
		filters[name]={}

		filters[name][priority]=[fn]
	}
	else{
		if (filters[name].hasOwnProperty(priority)){
			filters[name][priority].push(fn)
		}
		else{
			filters[name][priority]=[fn]
		}
	}
	if(file){
		ubications.push([name,file])
	}

}
function apply_filters(name,data,file){
	
	var content=null
	if (filters[name]){
		for (var priority of Object.keys(filters[name])){
			for (var fn of filters[name][priority]){
				
				content=fn(content,data)
			}
		}
	}
	if (file){
		historical.push([name,file])
	}
	
	
	return content

}
async function get_historical(){
	return historical
}
async function get_ubications(){
	return ubications
}
exports.add_filter=add_filter
exports.apply_filters=apply_filters
exports.filters=filters
exports.get_historical=get_historical
exports.get_ubications=get_ubications
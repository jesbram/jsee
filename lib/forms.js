const path = require('path');
const appRoot = require('app-root-path');

class Widget{
	constructor(attrs){
		if (attrs==undefined){
			attrs={"value":""}
		}
		this.attrs=attrs
		this.tagName="DIV"

	}
	value(value){

		if (value!=null && value!=undefined){

			this.attrs["value"]=value
		}
		
		return this.attrs["value"]
	}
	render(){
		return this.tag
	}
}

class TextInput extends Widget{
	constructor(attrs){
		super(attrs)
	}
	render(){
		var _attrs=""
		if (this.attrs["class"]==undefined){
			this.attrs["class"]=" form-control"
		}
		
		for (var elem in this.attrs){
			_attrs+=elem+"='"+this.attrs[elem]+"' "
			
		}
		return '<input '+_attrs+'>'
	}

}
class ColorInput extends Widget{
	constructor(attrs){
		super(attrs)
		
	}
	render(){
	
		var _attrs=""
	
		
		for (var elem in this.attrs){
			_attrs+=elem+"='"+this.attrs[elem]+"' "
			
		}
		return '<color-picker '+_attrs+'>'
	}

}
class Preview extends Widget{
	constructor(attrs){
		super(attrs)

	}
	render(){
		
		var _attrs=""
		
		if (this.attrs["multiple"]==undefined){
			this.attrs["multiple"]="true"
		}
		var l=[]
		if (typeof(this.attrs["value"])!="string"){
			for (var elem of this.attrs["value"]){
				l.push({"name":elem,"url":elem,"download":elem})
			}
		}
		else{

			l.push({"name":this.attrs["value"],"url":this.attrs["value"],"download":this.attrs["value"]})
		

		}

		this.attrs["filelist"]=JSON.stringify(l)
		for (var elem in this.attrs){
			_attrs+=elem+"='"+this.attrs[elem]+"' "
			
		}
		


		return '<input type="hidden" name=""><preview '+_attrs+'>'
	}

}
class CheckboxInput extends Widget{
	constructor(attrs){
		super(attrs)
	}
	render(){
		var _attrs=""
		if (this.value()){
			this.attrs["checked"]=true
		}
		else{
			if (this.attrs["checked"]){
				delete this.attrs["checked"]
			}
		
		}
		if (this.attrs["class"]==undefined){
			this.attrs["class"]=" form-control"
		}
		
		for (var elem in this.attrs){
			_attrs+=elem+"='"+this.attrs[elem]+"' "
			
		}

		return '<input '+_attrs+'>'
	}

}


class CodeInput extends Widget{
	render(){
		var _attrs=""
		this.attrs["style"]="text-align: left;width:100%;"
	
		for (var elem in this.attrs){
			_attrs+=elem+"='"+this.attrs[elem]+"' "
		}
	
		return "<inputcodemirror "+_attrs+">"
	}
}

class RichText extends Widget{
	render(){
		var _attrs=""
		
	
		for (var elem in this.attrs){
			if (elem!="value"){
				_attrs+=elem+"='"+this.attrs[elem]+"' "
			}
			
		}
	
		return "<textarea name='"+this.attrs["name"]+"' hidden>"+(this.attrs["value"]?this.attrs["value"]:"")+"</textarea><rich-text "+_attrs+">"
	}
}

class ObjectInput extends Widget{
	render(){
		var _attrs=""
		this.attrs["style"]="width:100%;text-align:left;"
	
		for (var elem in this.attrs){
			if (elem=="value"){
				_attrs+=elem+'="'+JSON.stringify(this.attrs[elem]).replace(/"/g,"&quot;")+'"'
				
			}else{
				_attrs+=elem+'="'+this.attrs[elem]+'"'
			}
		
		}
		
		return "<inputcodemirror "+_attrs+">"
	}
}
class PasswordInput extends Widget{
	constructor(attrs){
		super(attrs)
	}

}
class Select extends Widget{
	constructor(attrs){
		super(attrs)
	}
	render(){
		var _attrs=""
		var _choices=""
		
		if (!this.attrs["data-live-search"]){
			this.attrs["data-live-search"]="true"
		}
		if (!this.attrs["class"]){
			this.attrs["class"]="form-control"
			this.attrs["style"]="padding:0px;border:none;"
		}
		if (!this.attrs["multiple"]){
			this.attrs["multiple"]="false"	
		}

		if (this.choices){
			
			var string=(JSON.stringify(this.choices))

			this.attrs[":options"]=(JSON.stringify(this.choices)).replace(/"/g,"&quot;")
			
		}
		
		var value=this.attrs["value"]


		for (var elem in this.attrs){
			_attrs+=elem+'="'+this.attrs[elem]+'"'
		}

		/*
		for (var elem of this.choices){

			_choices+="<option value="+elem["value"]+"> "+elem["label"]+"</option>"
			
		}
		*/
		

		return '<form-select '+_attrs+' ></form-select>'
	}

}
class TextArea extends Widget{
	constructor(attrs){
		super(attrs)
	}

	render(){

		var _attrs=""

		if (this.attrs["class"]==undefined){
			this.attrs["class"]=" form-control"
		}

		for (var elem in this.attrs){

			_attrs+=(elem+"='"+this.attrs[elem]+"' ")
		}



		
		return '<textarea '+_attrs+'>'+(this.attrs["value"]?this.attrs["value"]:"")+'</textarea>'
	}

}
class Field{
	constructor(label,widget,meta){
		this.label=label
		this.widget=widget
		this.meta=meta//{validators,max_length,min_length,required}
		this.html_name=label
		this.id_for_label=label
		this.errors=[]
		this._value=null
		this.widget.field=this
		this.is_hidden=false
		this.label_style=""
		this.name=null

	}
	hidden(){
		this.is_hidden=true
		this.widget.attrs["hidden"]=""
	}
	show(){
		this.is_hidden=false
	}
	value(value){

		if (value!=null && value!=undefined){
			this._value=value
		}
		
		return this._value

	}
	validate(value){
        //Check if value consists only of valid emails.
        // Use the parent's handling of required fields, etc.
        return true
    }
    transform(value){
    	return value
    }
    destransform(value){
    	return value
    }
    prerender(){

    }
}
class CharField extends Field{
	constructor(label,widget,meta){
		super(label,widget,meta)
		
	}
	render(){
		
		this.widget.value(this.value())
		var style=this.label_style

		return (!this.is_hidden?("<label style='"+style+"' >"+this.label+"</label>"):"")+this.widget.render()
	}
	transform(value,name,instance){
		if (this.widget instanceof PasswordInput){
			if (instance[name]!=value){
				return this.generateHash(value)
			}
		}
	
    	return value
    }
}
class ObjectField extends Field{
	constructor(label,widget,meta){
		super(label,widget,meta)
		
	}
	transform(value){
		return JSON.parse(value)
	}
	render(){
		this.widget.value(this.value())
		var style=this.label_style
		return (!this.is_hidden?("<label style='"+style+"' >"+this.label+"</label>"):"")+this.widget.render()
	}
}
class ChoiceField extends Field{
	constructor(label,widget,meta){
		super(label,widget,meta)
	}
	render(){
		this.widget.value(this.value())
		var style=this.label_style

		return (!this.is_hidden?("<label style='"+style+"'>"+this.label+"</label>"):"")+this.widget.render()
	}
}
class MultipleChoiceField extends Field{
	constructor(label,widget){
		super(label,widget)
	
	}
	async transform(value,name,instance){
		var Apps=require("./apps")
		if (this.form._meta.model.schema.tree[name][0]){
			if (this.form._meta.model.schema.tree[name][0].ref){
				var app_model=this.form._meta.model.schema.tree[name][0].ref.split(".")
				var model=Apps.get_model_from_app(app_model[0],app_model[1])
			}
			else if (this.form._meta.model.schema.tree[name][0].type==String){

			}
			
		}

	
		
		var l=[]
		if (value){
			return value.split(",")
		}
		return []
	}
	render(){

		this.widget.value(this.value())


		return 	(!this.is_hidden?("<label style='display: block;width:100%;text-align:left'>"+this.label+"</label>"):"")+this.widget.render()
	}
}
class BooleanField extends CharField{
	constructor(label,widget){
		super(label,widget)
		
	}
	transform(value,name,instance){
		if (value=="on"){
			return true
		}
		else if (value=="off"){
			return false
		}
		else if (value=="0"){
			return false
		}
		else if (value=="1"){
			return true
		}
		else if (value=="false"){
			return false
		}
		else if (value=="true"){
			return true
		}
		return value
	}

}
class MultipleChoiceWidget extends Widget{
	constructor(attrs){
		super(attrs)
		
	}
	
	
	render(){
		var Apps=require("./apps")
		var _attrs=""
		//this.attrs[":data"]=[{label:"Etiqueta1",key:0,initial:"etiqueta1"},{label:"Etiqueta2",key:1,initial:"etiqueta2"}]
		var value=[]
		if (this.attrs[":data"]==undefined){
			this.attrs[":data"]=[]
		}
	
		if (this.attrs["value"]){
			for (var elem of this.attrs["value"]){

				for (var elem2 of this.attrs[":data"]){
		
					if (elem.toString()==elem2["initial"]){
						value.push(elem2["key"])
						
						break
					}
				}
			}

		}

		this.attrs["value"]=value
		

		for (var elem in this.attrs){
			if (typeof(this.attrs[elem])=="string"){
				_attrs+=elem+'="'+this.attrs[elem]+'"'
			}else{

				_attrs+=elem+'="'+JSON.stringify(this.attrs[elem]).replace(/"/g,"&quot;")+'"'
			}
			
		}
		var btns=""
		var scheme_field=this.field.form._meta.model.schema.tree[this.attrs.name][0]
		if (scheme_field && scheme_field.ref){

			var app_model=scheme_field.ref.split(".")
			var url="../../../"+app_model[0]+"/"+app_model[1]+"/new"
			var btns="<div class='multiple-choice-widget-btns'><button data-href='"+url+"' class='btn btn-success btn-add' type='button'>+</button><button class='btn btn-danger btn-remove' type='button'>-</button></div>"
		}
		else if (this.attrs["url"]){

			var btns="<div class='multiple-choice-widget-btns'><button data-href='"+this.attrs["url"]+"' class='btn btn-success btn-add' type='button'>+</button><button class='btn btn-danger btn-remove' type='button'>-</button></div>"
		}
		
		
		
		return 	"<multiple-choice-widget "+_attrs+"></multiple-choice-widget>"
		
	}
	
}

class ModelForm{

	constructor(instance,data,meta){
		this.fields={}
		this.data=data
		this.instance=instance
		this._meta=meta
		if (this._meta["widgets"]==undefined){
			this._meta["widgets"]={}
		}
		this.valid=true
		if(this._meta.hidden_fields==undefined){
			this._meta.hidden_fields=[]
		}
		this.private=[]
		this.upload_format="datetime"


		var that=this
		
		function append_field(that,field){
			var Apps=require("./apps")
			if ( that._meta.hasOwnProperty("widgets") && that._meta.widgets.hasOwnProperty(field)){
						
				that.fields[field]=new CharField(field,that._meta.widgets[field])
				that.fields[field].form=that
			}
			else{

				if (that._meta.model.schema.tree[field][0]==undefined){
				
					if (that._meta.model.schema.tree[field].hasOwnProperty("type")){
						
						if ( that._meta.model.schema.tree[field]["type"]==String){
							
							that.fields[field]=new CharField(field,new TextInput({"name":field}))
							that.fields[field].form=that
						}
						else if (that._meta.model.schema.tree[field]["type"]==Number){
						
							that.fields[field]=new CharField(field,new TextInput({"name":field,"type":"number"}))
							that.fields[field].form=that
						
						}
						else if (that._meta.model.schema.tree[field]["type"]==Boolean){
						
							that.fields[field]=new BooleanField(field,new CheckboxInput({"name":field,"type":"checkbox"}))
							that.fields[field].form=that
							
						}
						else if (that._meta.model.schema.tree[field]["type"]==Object && that._meta.model.schema.tree[field].ref==undefined){
						
							that.fields[field]=new ObjectField(field,new ObjectInput({"name":field}))
							that.fields[field].form=that
							
						}
						
						else if (that._meta.model.schema.tree[field].ref!=undefined){
						
							
							that.fields[field]=new  ChoiceField(field,new Select({"name":field}))	
							that.fields[field].form=that
							

						}
					}
					else if (that._meta.model.schema.tree[field]==String){
						that.fields[field]=new CharField(field,new TextInput({"name":field}))
						that.fields[field].form=that
					}
					else if (that._meta.model.schema.tree[field]==Number){
						that.fields[field]=new CharField(field,new TextInput({"name":field,"type":"number"}))
						that.fields[field].form=that
					}
					else if (that._meta.model.schema.tree[field]==Boolean){
						that.fields[field]=new BooleanField(field,new CheckboxInput({"name":field,"type":"checkbox"}))
						that.fields[field].form=that
					}
				}
				else if (that._meta.model.schema.tree[field].length==1){
					console.log("rrrrrrrr")
					if (that._meta.model.schema.tree[field][0].ref){
				
						that.fields[field]=new  MultipleChoiceField(field,new MultipleChoiceWidget({"name":field}))	
						that.fields[field].form=that
					}
					else{
						that.fields[field]=new  MultipleChoiceField(field,new MultipleChoiceWidget({"name":field}))	
						that.fields[field].form=that

					}
					
				}
				else{
					console.log("yyyyyyyyasdasdy",field,that._meta.model.schema.tree[field][0])
				}

				
				
			
				
			}
			

		}


		
		if (that._meta.fields=="__all__"){

			for (var field of Object.keys(that._meta.model.schema.tree)){
				append_field(that,field)
			}
			
			
		}
		else if (that._meta.fields){//"__all__"
			
			for (var field of that._meta.fields){
				append_field(that,field)		
			}
		}
		
	}
	async build(){
		var Apps=require("./apps")
		var that=this
		for (var field in this.fields){

			if (that._meta.model.schema.tree[field][0]==undefined){
		
				if (this._meta.model.schema.tree[field].ref!=undefined){

					var app_model=this._meta.model.schema.tree[field].ref.split(".")
									
					var model=Apps.get_model_from_app(app_model[0],app_model[1])
					var data=await model.find({},"_id name").exec()

					var choices=[]
					for (var i = 0; i < data.length; i++) {

						choices.push({label:await data[i].toString(),value:data[i]["_id"]})
					}
					this.fields[field].widget.choices=choices
				}
			}
			else{

					if (this._meta.model.schema.tree[field][0].ref){
						var app_model=this._meta.model.schema.tree[field][0].ref.split(".")
								
						var model=Apps.get_model_from_app(app_model[0],app_model[1])
						var data=await model.find({},"_id name").exec()

						var choices=[]
				
						
						for (var i = 0; i < data.length; i++) {
							var string=await data[i].toString()
							if (!(string.startsWith("{") && string.endsWith("}"))){
								choices.push({label:string,key:data[i]["_id"],initial:data[i]["_id"]})
							}else{
								choices.push({label:data[i]["_id"],key:data[i]["_id"],initial:data[i]["_id"]})
							}
							
						}

						this.fields[field].widget.attrs[":data"]=choices

					}
					else{
						console.log("Hay que estudiar mas esto porque build hasta ahora solo sirve para construir configuraciones para relaciones")
						var app_model=this._meta.model.schema.tree[field][0]
						if (this._meta.model.schema.tree[field][0].type==String){
							var data=this.instance[field]
							var choices=[]
							for (var i = 0; i < data.length; i++) {
								string=await data[i].toString()
								choices.push({label:string,key:string,initial:string})
								
								
							}
							
							this.fields[field].widget.attrs[":data"]=choices
						}
						
						/*
								
						var model=Apps.get_model_from_app(app_model[0],app_model[1])
						var data=await model.find({},"_id name").exec()

						var choices=[]
						string=await data[i].toString()
						for (var i = 0; i < data.length; i++) {

							if (!(string.startsWith("{") && string.endsWith("}"))){
								choices.push({label:string,key:data[i]["_id"],initial:data[i]["_id"]})
							}else{
								choices.push({label:data[i]["_id"],key:data[i]["_id"],initial:data[i]["_id"]})
							}
							
						}

						this.fields[field].widget.attrs[":data"]=choices
						*/

					}
			
			}
		}
		console.log("eeeeeeeeee")
		
						
	}
	async upload(field,buffer){
		const fs=require("fs")
		var that=this
		async function build(field,buffer){
			var file=buffer.name.split(".")
			var ext=file.slice(-1)
		
			if (that.upload_format=="datetime"){
				var date=new Date()

			
				var date_path=date.getFullYear()+"/"+date.getMonth()+"/"+date.getDay()+"/"
				var file_path=date_path+file.slice(0,-1).join(".")
			}
			
			var n=1
			try {
			 var upload_path="../public/media/"
			 if (that.private.indexOf()>-1){
			 	var upload_path="../private/"
			 	
			 }
			 var _path=path.join(appRoot.path,"lib/"+upload_path+date_path)
		
			 await fs.mkdirSync(_path, { recursive: true })
			 var tmp=upload_path+date_path+file+file.slice(0,-1).join(".")
			 var _file_path=file_path
			 while (true){
			 	console.log(__dirname +"/"+upload_path+file_path+"."+ext,fs.existsSync(__dirname +"/"+upload_path+file_path+"."+ext))
			 	if (fs.existsSync(__dirname +"/"+upload_path+_file_path+"."+ext)){
			 		_file_path=file_path+"("+n+")"
			 		n+=1
			 	}
			 	else{
			 		break
			 	}
			 } 
			 file_path=_file_path

			 file_path+"."+ext
		    
		      fs.writeFile(__dirname +"/"+upload_path+file_path+"."+ext, buffer.data, function(err, result) {
		      	if(err) console.log('error', err);
		      });
		      
		    } catch (e) {
		      console.log(e)
		      console.error("Could not upload file '"+buffer.name+"'")
		     

		    } 
		    if (that.private.indexOf()>-1){
		    	return "/"+file_path+"."+ext
		    }
		    else{
		    	return "/media/"+file_path+"."+ext
		    }
		}

		if (buffer.data){
			return await build(field,buffer)
		}
		else{//multiple
			var l=[]
			for (var elem of buffer){
				l.push(await build(field,elem))
			}

			return l

		}


		
	}
	render(){
		/*Este metodo es usado para devolver el formulario*/
		var _form=""
	
		for (var field in this.fields){
			this.fields[field].widget.attrs["name"]=field
			if (this.instance){
				this.fields[field].value(this.instance[field])
			}
			if (this._meta.hidden_fields.indexOf(field)>-1){
				this.fields[field].hidden()
			}
					
			_form+="<div class='form-group row'>"+this.fields[field].render()+"</div>"

			console.log("aaaaaaa")
		}
		return _form

	}
	async save(options){

		if (this.instance){//actualiza 
			var data={}
			for (var elem in this.data){
				if (this.fields[elem]){
					data[elem]=await this.fields[elem].transform(this.data[elem],elem,this.instance)
				
					if (!this.fields[elem].validate(this.data[elem])){

						this.valid=false
					}
				}

				
			}
			if (this.valid){
				
				for (var elem in options.files){
					var files=await this.upload(elem,options.files[elem])
					console.log("fffffff")
					if (this.data["__"+elem]){
					 var array=this.instance[elem]

					 for (var elem2 in this.data["__"+elem].split(",")){
					 	i=array.indexOf(elem2)
					 	if (i>-1){
					 		array[i].splice(i,1)
					 	}
					 	files=array.concat(files)
					 }
					 
					
					}
									this.instance[elem]=files
				}

				for (var elem in data){
					this.instance[elem]=data[elem]
				}
							

				
				
				

				await this.instance.save()
				
			}

			
		}
		else{//crea
	
			for (var elem in this.data){
				if (this.fields[elem]){
					this.data[elem]=this.fields[elem].transform(this.data[elem],elem)
					if (!this.fields[elem].validate(this.data[elem])){
						
						this.valid=false
					}
				}
			}

			if (this.valid){
				
				if (options.files){
					for (var file in options.files){
					
						this.data[file]=await this.upload(file,options.files[file])
					}
				}

				
				var instance=new this._meta.model(this.data)
				
				await instance.save()


			}
			
			

		}
		
	}
	[Symbol.iterator](){
		var index = -1;

	    return {
	      next: () => ({ value: this.fields[++index], done: !(index in this.fields) })
	    };
	  };
	as_table(){
		var _form="<table>"

		for (var field in this.fields){
			
			_form+="<tr class='form-group row'>"+this.fields[field].render()+"</tr>"
		}
		return _form+"</table>"
		
	}
}


class Form{
	constructor(instance,data,meta){
		this.fields={}
		this.data=data
		this.instance=instance
		this._meta=meta
		this._builded=false
		if(this._meta.hidden_fields==undefined){
			this._meta.hidden_fields=[]
		}
	}
	render(){
		
		/*Este metodo es usado para devolver el formulario*/
		if ( this._meta.hasOwnProperty("widgets")){			
			for (var field in this._meta.widgets){
				this.fields[field].widget=this._meta.widgets[field]
				
			}
		}
		
		for (var field in this.fields){

			this.fields[field].widget.attrs["name"]=field
		}
	
		var _form=""
		if (this._meta.fields=="__all__"){
			
			for (var field in this.fields){
				
				_form+="<div>"+this.fields[field].render()+"</div>"
			}
			
		}
		else if (this._meta.fields){//"__all__"
			
			for (var field in this.fields){
				if (this._meta.fields.indexOf(field)>-1){
					_form+="<div>"+this.fields[field].render()+"</div>"
				}
				
			}
		}
		
		return _form
		

	}
	save(){
		if (this.instance){//actualiza 
			for (var elem in this.data){
				this.instance[elem]=this.data[elem]
			}
			this.instance.save()
		}
	
		
	}
	as_table(){
		/*Este metodo es usado para devolver el formulario*/
		if ( this._meta.hasOwnProperty("widgets")){			
			for (var field in this._meta.widgets){
				this.fields[field].widget=this._meta.widgets[field]
				
			}
		}
		for (var field in this.fields){

			this.fields[field].widget.attrs["name"]=field
		}
		var _form="<table>"
		if (this._meta.fields=="__all__"){
			
			for (var field in this.fields){
				
				_form+="<tr>"+this.fields[field].render()+"</tr>"
			}
			
		}
		else if (this._meta.fields){//"__all__"
			
			for (var field in this.fields){
				if (this._meta.fields.indexOf(field)>-1){
					_form+="<tr>"+this.fields[field].render()+"</tr>"
				}
				
			}
		}
		
		return _form+"</table>"
		

	}
		
	

}


/*
class newForm extends ModelForm {
	Meta=class {
		model=Model
		}

	constructor()
		super()
	}

}*/
exports.ModelForm=ModelForm
exports.Form=Form
exports.TextInput=TextInput
exports.ColorInput=ColorInput
exports.TextArea=TextArea
exports.Select=Select
exports.CharField=CharField
exports.MultipleChoiceField=MultipleChoiceField
exports.ChoiceField=ChoiceField
exports.MultipleChoiceWidget=MultipleChoiceWidget
exports.Preview=Preview
exports.RichText=RichText
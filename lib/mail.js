const nodemailer = require('nodemailer');


exports.sendEmail = function(req, res){
	var config=require("../config/")

	const transporter = nodemailer.createTransport({
		host: config.mail.host,
	 	port: config.mail.port,
	  	auth: {
	    	user: config.mail.user,
	    	pass: config.mail.pass
		  }
	});
	
	var mailOptions = {
	    from: config.mail.user,
	    to: req.user,
	    subject: req.subject,
		html: req.text
	};
	transporter.sendMail(mailOptions, function(error, info){
	    if (error){
	        return res.status(500).send(error.message);
	    } else {
			res.status(200).json({sucess: true, status: 200, email: req.body, msg: req.msg});
			console.log('Email enviado: ' + info.response);
	    }
	});
};
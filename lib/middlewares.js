
const fs = require("fs");
const express = require('express');
var sassMiddleware = require('node-sass-middleware');
var methodOverride = require('method-override')
const bodyParser = require('body-parser');
const settings = require("../settings");
const path = require('path');
const appRoot = require('app-root-path');
const fileUpload = require('express-fileupload');
var apps = require("./apps").apps;



exports.middlewares = function (server,app) {
	
	server.set('views', path.join(appRoot.path, 'views'));
	var views=[]
	server.use("/static",express.static(path.join(appRoot.path,"public")));
	for (var _app of apps){
		server.use("/static",express.static(path.join(appRoot.path,"apps",_app.name,"static")));
		views.push(path.join(appRoot.path,"apps",_app.name,settings.templates_folder))
		// Middleware
		server.use(bodyParser.json());
		server.use(methodOverride(function (req, res) {
		  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
		    // look in urlencoded POST bodies and delete it
		    var method = req.body._method
		    delete req.body._method
		    return method
		  }
		}))

		server.use(sassMiddleware({
		  src: path.join(appRoot.path,"apps",_app.name,"static",_app.name, 'scss'),
		  dest: path.join(appRoot.path,"apps",_app.name,"static",_app.name, "css"),
		  indentedSyntax: true, // true = .sass and false = .scss
		  sourceMap: true
		}));

	}
	const appDir = path.dirname(require.main.filename);
	server.use(fileUpload());


	server.use((request, response, next) => { 
	    response.locals.settings=settings;	  
    	response.locals.include=function(view,data={}){

    		ejs = require('ejs')
    	
    		if (server._settings.engine_view=="ejs"){
	    		
	    		if (!view.endsWith(".ejs"))
	    			view=view+".ejs"
	    		var _data=Object.assign(data,server.locals,response.locals)
	    		_data["settings"]=server._settings

	    	 	for (var _path of server.locals.settings.views){		    	 	
		    	 	if (fs.existsSync(path.join(_path, view))){		    	 		
		    	 		return ejs.renderFile(path.join(_path, view),_data,{async:false})._rejectionHandler0
		    	 	}		    	 	
		    	}
	    	}	    	 
	    }
	    return next();
	});
	views.push(path.join(appRoot.path, 'public',settings.templates_folder))
	
	server.set('views',views );
}
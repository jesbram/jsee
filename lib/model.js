const mongoose = require('mongoose')
const path = require('path');

const appRoot = require('app-root-path')
var Apps=require(path.join(appRoot.path,"lib/apps.js"))
function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }
function get_refs(instance,field){
	var l=[]
	for (var field in instance.schema.tree){
		if (instance.schema.tree[field]["type"]==mongoose.Schema.Types.ObjectId){
			l.push(field)
		}
	}
	return l


}
function get_fields(instance){
	var l=[]
	for (var field in instance.schema.tree){
		l.push(field)
	}
	return l


}
var fields={}
function add_fields(newfields){
	 
	 for (var elem in newfields){
	 
	 	if (fields[elem]){
	 		for( var elem2 in newfields[elem]){
	 			if (fields[elem][elem2]){
	 				fields[elem][elem2]=Object.assign(fields[elem][elem2],newfields[elem][elem2])
	 			}
	 			else{
	 				fields[elem][elem2]=newfields[elem][elem2]
	 			}

	 		}
	 		
	 	}
	 	else{
	 		fields[elem]=newfields[elem]
	 	}
	 }


}

function Field(path,name,data){
	if (fields[path] && fields[path][name]){
		return Object.assign(fields[path][name], data)
	}
	else{
		return data
	}
}
function Fields(path,data){

	if (fields[path]){
	
		return Object.assign(data,fields[path])
	}
	else{
		return data
	}


}
methods={}
function add_methods(newmethods){
	 
	 for (var elem in newmethods){
	 
	 	if (methods[elem]){
	 		methods[elem].append(newmethods[elem])
	 	}
	 	else{
	 		methods[elem]=[newmethods[elem]]
	 	}
	 }


}
function Methods(path,schema){

	if (methods[path]){
		for (var elem of methods[path]){
			elem(schema)
		}
	}


}
function is_ref(instance,field){
	if (instance.schema.tree[field]["type"]==mongoose.Schema.Types.ObjectId){
		return true
	}
	return false
}
function create_instance(data,){
	var obj={
		"__data__":data,

	}
	for (var field in instance.schema.tree){

	}
	
}



function Set(model,data){
	
	var obj={
		"_model":model,
		"_data":data,
		"_ref":get_refs(model),
		"all":function(){
			console.log("ttttttt",this._data)
		},
		"get":function(){

		},
		"filter":function(){

		}
		}

	var fields=get_fields(model)
	function* recursive(){	
		let c=0;
		
		while (c<obj._data.length) {
			var obj2={}
			var properties={}
			for (var field of fields){
				function get(field){
					return function(){ 
						
						if (obj._ref.indexOf(field)>-1){
							var ref=obj._model.schema.tree[field]["ref"].split(".")
							var app=ref[0]
							var model=ref[1]
							
							var model=Apps.get_model_from_app(app,model)
							
							var instance=ModelInstance(model,[obj._data[c]["_id"]])							
							return instance.objects
						}
						else{
							return obj._data[c][field];
						}
					 
					}
				}
				function set(field){
					return function(value){
						obj._model.update({_id:obj._data[c]["_id"]},{$set:{field:value}})
					}
				}
				properties[field]={
					get :get(field),
			    	set :set(field)
				}
				
			}
			
			Object.defineProperties(obj2,properties)
			
			obj2["save"]=function(){}
			obj2["delete"]=function(){}
			yield obj2;
			c+=1
		}
		
		
		
	}
	let handler = {
			  get(target, name) {
			  	
			  	if (isNumber(name)){
			  		return target._data[parseInt(name)]

			  	}
			  	else{
			  		return `Value for attribute ${name}`
			  	}
			    
			  }
			}

	
	obj = new Proxy(obj, handler);
	obj[Symbol.iterator]=recursive
	return obj
}	

class Manager{
	constructor(model,refs){
		this.model=model
		this.refs=refs

		
	}
	find(consulte,options,fn){
		if (this.refs){
			consulte["$in"]=this.refs
		}
		var that=this
		return this.model.find(consulte,function(err,data){
			fn(err,Set(that.model,data))
		})
	}
	get(consulte,options,fn){
		if (this.refs){
			consulte["$in"]=this.refs
		}
		
		return new Set(this.model,[this.model.findOne(consulte,options)])
	}
	all(fn){
		console.log("uuuuuuuuuuuu")
		return this.find({},{},fn)
	}
	
	create(){

	}
	delete(){

	}
	save(){

	}
}
function ModelInstance(model,ref//ref por defecto es null tiene valor cuando es traido desde una instancia
	){
	model.objects=new Manager(model,ref)
	return model
			
	
}

class Model{
	constructor(data){
		this.schema = new mongoose.Schema(data)
	}

}

/*

model=ModelInstace(User)
#model.objects.get({email:email})
instance=model.objects.find({email:email})

#instance.__response=
instance.fieldname

*/

exports.ModelInstance=ModelInstance
exports.get_fields=get_fields
exports.Field=Field
exports.Fields=Fields
exports.add_fields=add_fields
exports.Methods=Methods
exports.add_methods=add_methods

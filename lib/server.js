const express = require('express');
const expressWinston = require('express-winston');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const fs = require("fs");
const appRoot = require('app-root-path');
let app = express()
app.set('trust proxy', true);
app.config=require("../config/")
const settings = require('../settings');
const mongoose = require('mongoose')
app.set('view engine', settings.view_engine);
global["cache"]={}
const winstonInstance = require('../config/winston');
var share_socket=null
if (app.config.central_addr  && app.config.socket_options)
	var share_socket = require('socket.io-client')(app.config.central_addr, app.config.socket_options)

function msj_error(msj){
	return '\x1b[41m'+msj+"\x1b[0m"
}

const signals = require('./signals');
const filters = require('./filters');




/*
generar las rutas de importacion necesarias para webpack
es decir generar rutas estaticas
*/


// Save the string "Hello world!" in a file called "hello.txt" in
// the directory "/tmp" using the default encoding (utf8).
// This operation will be completed in background and the callback
// will be called when it is either done or failed.


var start='function load_library(app,path){\n'
for (var elem of settings.apps){
	start+="if (app=='"+elem+"'){ return require('Root/"+elem+"/static/'+path)}\n"
}
start+="}\nexports.load_library=load_library;\n"
fs.writeFile(__dirname+'/../public/generates/generate_webcpack_require.js', start, function(err) {
  // If an error occurred, show it and return
  if(err) return console.error(err);
  // Successfully wrote to the file!
});



async function check_permissions(auth,instance){
	var Apps=require("../lib/apps")
	var get_apps=Apps.get_apps
	var ContentType=auth.get_model("content_type")
	var Permission=auth.get_model("permission")
	for (var instance of get_apps()){

		for (var row of instance.get_models()){
			var model=row[0]
			var _row=row[0].modelName.split(".")
			
			var content_types=await ContentType.find( {"app_label":_row[0],"model":_row[1]})
		
			if (content_types.length==0){

				content_type=new ContentType({"app_label":_row[0],"model":_row[1]})
				content_type.save()

			}
			else{
				//si hay content_types
				for (var elem of content_types){
					//si el content_type concide con las aplicaciones y el modelo actual
					if (elem["app_label"]==_row[0] && elem["model"]==_row[1]){
						//antes de crear el permiso evaluo s
						if (model.meta && model.meta.permissions){
							for (var perm of model.meta.permissions){
								await Permission.create({"content_type":content_type["_id"],
									"name":perm[0],
									"codename":perm[1]})
							}
						}
						break
						
					
					}
					

				}
				
				
			}
		
		}
	}
}



module.exports=function(port){
	global.__rootdir = __dirname;

	var server = require('http').Server(app);

	if (app.config.private_key && app.config.certificate) {

		function open (p) {
			return fs.readFileSync(path.resolve(global.__rootdir, p))
		}

		server = require('https').createServer({
			key: open(app.config.private_key),
			cert: open(app.config.certificate)
		}, app)

	} 
	else {

		server = require('http').createServer(app)

	}
	var server2= app.listen(port, () => {
	  console.log(`Example app listening at http://localhost:${port}`)
	})
	var socket_io = require('socket.io')(server2);
	var socket_io = require('socket.io')(server2);

	app.config.env="development"
	app.use(function (req, res, next) {

		res.locals["do_signal"]=signals.do_signal
		res.locals["apply_filters"]=filters.apply_filters
		next();

	})
	

	var Apps=require("../lib/apps")
	var get_apps=Apps.get_apps
	var get_app=Apps.get_app
	var register_socket=Apps.register_socket
	var register_model=Apps.register_model
	var get_all_models=Apps.get_all_models
	var add_fields=require("../lib/model").add_fields
	var add_methods=require("../lib/model").add_methods
	for (var _app of settings.apps){ 
		
		//Esta clase de archivos es usados para ampliar los campos de los esquemas del modelo
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/fields.js'))){
			
			add_fields(require(path.join(appRoot.path,"apps/"+_app+'/fields.js')))
		}
	}
	for (var _app of settings.apps){ 
		
		//Esta clase de archivos es usados para ampliar los campos de los esquemas del modelo
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/methods.js'))){
			
			add_methods(require(path.join(appRoot.path,"apps/"+_app+'/methods.js')))
		}
	}
	/*
	if (app.config.env === 'development') {
	  app.use(morgan('combined', { stream: winstonInstance.stream }));
	}
	*/

	// log error in winston transports except when executing test suite
	if (app.config.env !== 'test') {
	  app.use(expressWinston.errorLogger({
	    winstonInstance
	  }));
	}



	
	

	for (var _app of settings.apps){ 
		var _instance=require("../apps/"+_app+'/app')
		var instance=new _instance(_app,app)
		instance.ready()
		



		instance.add_signal=signals.add_signal
		instance.do_signal=signals.do_signal
		instance.add_filter=filters.add_filter
		instance.apply_filters=filters.apply_filters


		
		
		

		if (!fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/routes')))
			throw (msj_error("No tienes un archivo de rutas en: "+"apps/"+_app+'/routes/index.js'))
	
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/models')))
			fs.readdirSync(path.join(appRoot.path,"apps/"+_app+"/models/")).forEach(file => {
				if (!file.endsWith(".example.js")  && file!=".gitkeep")
					register_model(_app,require(path.join(appRoot.path,"apps/"+_app+"/models/"+file)))
			});
		
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/signals.js'))){
			var m=require(path.join(appRoot.path,"apps/"+_app+'/signals.js'))
			m(app,instance)
		}
			
		
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/filters.js'))){
			var m=require(path.join(appRoot.path,"apps/"+_app+'/filters.js'))
			m(app,instance)
		}
		for (var middlewares of settings.middlewares){
			if (middlewares.startsWith(_app+"/")){
				var mid=require("../apps/"+ middlewares)
				mid.middlewares(app,instance);
				break
			}
			
			
		}
	

			


		router = require("../apps/"+_app+'/routes')(app,instance,null);
		if (!fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/app.js')))
			throw (msj_error("No tienes un archivo de rutas en: "+"apps/"+_app+'/app.js'))
		if (router==undefined)
			throw (msj_error("La rutas del la aplicacion '"+_app+"' debe retornar el objeto 'router' por favor"))
		
		
		require("../lib/apps").add_app(instance)
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app+'/admin.js'))){
			var m=require(path.join(appRoot.path,"apps/"+_app+'/admin.js'));
			m(app,instance)
		
		}

			
		
	}


	var auth=get_app("auth")
	for (var _app of get_apps()){
		//estos archivos tienen la finalidad si existen de dotar los modelos de
		//otras aplicaciones de metodos que tienen que ver con la aplicacion actual
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app.name+'/models.js')))
			require(path.join(appRoot.path,"apps/"+_app.name+'/models.js'))(_app);
		// Create HTTP & WebSocket servers
		
		if (fs.existsSync(path.join(appRoot.path,"apps/"+_app.name+'/sockets' ))){

			fs.readdirSync(path.join(appRoot.path,"apps/"+_app.name+"/sockets/")).forEach(file => {
				if (!file.endsWith(".example.js") && file!=".gitkeep"){
					
					var socket=require("../apps/"+_app.name+'/sockets/'+file )
					
				
					register_socket(_app.name,file,socket(app,_app,socket_io,share_socket))

				}
					
					

			})
		}


		
			
		}
	check_permissions(auth)	
	
	
	
	

	app._settings=settings
	var mid=require("../lib/middlewares")
	mid.middlewares(app);
	app.get_app=function (name) {
		for (var _app of get_apps()){
			if (_app.name==name){
				return _app
			}
		}
		// body...
		console.log("La app '"+name+"' no esta habilitada")
	}
	// Alternativamente servir archivos
	app.use(express.static(path.join(appRoot.path, 'public')))
	app.use("/wiki",express.static(path.join(appRoot.path, 'doc/build/html/')))

	app.listen(function(req,res){

	})


	app.auto_routes=function (){
		if (settings.auto_routes){
			for (var elem of get_apps()){
				
				app.use("/"+elem.name+"/",elem.load_router())
			}
		
		app.get("/",(req,res)=>{
			

			res.render(settings.index_view)
		})
		
		}
		

	}
	app.auto_apis=function (){
		if (settings.auto_apis){
			for (var elem of get_apps()){
				console.log("/json/"+elem.name+"/")
				if (fs.existsSync(path.join(appRoot.path,"apps/"+elem.name+'/apis/index.js' ))){
					app.use("/json/"+elem.name+"/",elem.load_apis())
				}
			}
	
		}
		

	}
	app.auto_sockets=function (){
		if (settings.auto_apis){
			for (var elem of get_apps()){
				if (fs.existsSync(path.join(appRoot.path,"apps/"+elem.name+'/sockets/index.js' ))) {
					app.use("/sockets/"+elem.name+"/",elem.load_sockets(elem.name))
				}
			}
	
		}
		

	}
	app.set("port",process.env.PORT||3000)

	// make bluebird default Promise
	Promise = require('bluebird'); // eslint-disable-line no-global-assign

	// plugin bluebird promise in mongoose
	mongoose.Promise = Promise;

	// connect to mongo db   
	const mongoUri = app.config.mongo_host;
	mongoose.connect(mongoUri, { useNewUrlParser: true})
	    .then(db => winstonInstance.info(`${path.basename(__filename)}: Servidor conectado a ${mongoUri}`))
	    .catch(err => winstonInstance.error(`${path.basename(__filename)}: Error conectando a ${mongoUri}`));

	// print mongoose logs in dev env
	if (app.config.MONGOOSE_DEBUG) {
	  mongoose.set('debug', (collectionName, method, query, doc) => {
	    debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
	  });
	}



	app.load_router=function(name){
		for (var _app of settings.apps){ 
			for (var _app2 of get_apps()){ 
				if (_app==name  && _app2.name==name){
					return _app2.load_router()

				}
			}
		}
		return function(req, res){
			res.send('what???', 404);
		}

	}
	return app
	
}

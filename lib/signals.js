var signals={}
var historical=[]
var ubications=[]
function add_signal(name,fn,priority,file){
	if (priority==undefined){
		priority=10
	}

	if (!signals.hasOwnProperty(name)){
		signals[name]={}
		signals[name][priority]=[fn]
	}
	else{
		if (signals[name].hasOwnProperty(priority)){
			signals[name][priority].push(fn)
		}
		else{
			signals[name][priority]=[fn]
		}
	}
	if(file){
		ubications.push([name,file])
	}

}
async function do_signal(name,data,file){
	if (signals[name]){
		for (var priority of Object.keys(signals[name])){
			for (var fn of signals[name][priority]){
			
				await fn(data)
			}
		}
	}
	if (file){
		historical.push([name,file])
	}


}
async function get_historical(){
	return historical
}
async function get_ubications(){
	return ubications
}
exports.add_signal=add_signal
exports.do_signal=do_signal
exports.get_historical=get_historical
exports.get_ubications=get_ubications
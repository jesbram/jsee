/*
var obj=[{"name":1},
{"name":2},
{"name":3},
{"name":4},
{"name":5},
]
var c=0
function* recursive(){
	
	while (c<obj.length) {
		yield obj[c];
		c+=1
	}
	
	
}
obj[Symbol.iterator]=recursive


for (var elem of obj){
	console.log(elem)
}

({
  get * (property) {
    // handle property gets here
  }
})
*/
let handler = {
  get(target, name) {
    return `Value for attribute ${name}`
  }
}

let x = new Proxy({}, handler);
console.log(x.lskdjoau); // produces message: "Value of attribute 'lskdjoau'"
import  settings from  "System/settings.js"

var vueapps={}
var libraries=[]
var warnings=[]
window.__VUEAPP_SIGNALS__={}
var sockets={}
window.__VUEAPP_VARIABLES__={"__LIBRARIES__":{},"__SOCKETS__":{}}

function add_signal(name,fn,priority){
	if (priority==undefined){
		priority=10
	}

	if (!window.__VUEAPP_SIGNALS__.hasOwnProperty(name)){
		window.__VUEAPP_SIGNALS__[name]={}
		window.__VUEAPP_SIGNALS__[name][priority]=[fn]
	}
	else{
		if (window.__VUEAPP_SIGNALS__[name].hasOwnProperty(priority)){
			window.__VUEAPP_SIGNALS__[name][priority].push(fn)
		}
		else{
			window.__VUEAPP_SIGNALS__[name][priority]=[fn]
		}
	}

}
function do_signal(name,data){
		for (var signal in window.__VUEAPP_SIGNALS__){
			for (var priority in window.__VUEAPP_SIGNALS__[signal]){
				for (var fn of window.__VUEAPP_SIGNALS__[signal][priority]){
					fn(data)
				}
			}
		}

	}


class VueApp{

	constructor(name){
		this.name=name
		this.routes=[]
		this.vueapps=vueapps
		this.warnings=warnings
		this.signals=window.__VUEAPP_SIGNALS__
	}

	load_view(app,route){
		
		//return require("Root/"+app+"/"+settings.templates_folder+route)
		
	}
	/*
	add_route(route,view,props=true){
		this.routes.push({"path":route,component: () =>view,props:props})
	}
	*/
	init_routes(){
	  vueapps[this.name]=[]
	  //carga el modulo de vue de cada aplicacion
	  if (settings.vue_hooks[this.name] && settings.apps.indexOf(this.name)>-1 ){

	    for (var hook of settings.vue_hooks[this.name]){
	      
	      var modulo=require("Public/generates/generate_webcpack_require.js")
	      
	      modulo=modulo.load_library(hook[0],"components/"+hook[1])
	      
	      vueapps[this.name].push(modulo)
	    }
	  }
	    
	  
	  var routes=[]
	  
	  for (var app of vueapps[this.name]){
	    routes=routes.concat(app.routes)
	  }
	  return routes

	}
	init_signals(){
	
		  //carga el modulo de vue de cada aplicacion
		  if (settings.vue_signals[this.name] && settings.apps.indexOf(this.name)>-1 ){

		    for (var hook of settings.vue_signals[this.name]){
		      //cargarlo asi deberia ser suficiente
		      var modulo=require("Public/generates/generate_webcpack_require.js").load_library(hook[0],hook[1])
		    }
		  }
		    
		
	
	}
	add_signal(name,fn,priority){
		add_signal(name,fn,priority)
	}
	do_signal(name,data,file){
		do_signal(name,data)
		if (file && window.settings.mode=="debug"){
			fetch("/json/dashboard/report-signal", {
			   method: 'PATCH',
			   body: {"signal":"name","file":file}
			})
			.then(function(response) {
			   if(response.ok) {
			       return response.text()
			   } else {
			       throw "Error en la llamada Ajax";
			   }

			})
			.then(function(texto) {
			   console.log(texto);
			})
			.catch(function(err) {
			   console.log(err);
			});
		}

		
	}
	load_signals(source){
		var that=this
		that.get_socket
		that.add_signal=this.add_signal
		that.do_signal=this.do_signal
		that.load_library=this.load_library
		that.get_socket=this.get_socket
		that.get_signals=this.get_signals
		require("Public/generates/generate_webcpack_require.js").load_library(this.name,source).default(that,sockets)
	}
	register_socket(name,socket,overwrite){
		if (!this.getvar("__SOCKETS__").hasOwnProperty(name)){
			console.log("Socket: "+name+" registrado")
			this.getvar("__SOCKETS__")[name]=sockets
		}
		if (overwrite){
			console.log("Socket: "+name+" sobrescrito")
			this.getvar("__SOCKETS__")[name]=sockets
		}
	}
	get_socket(name){
		return this.getvar("__SOCKETS__")[name]
	}


	load_library(app,name){
		
		//carga el modulo de vue de cada aplicacion
		
		if ( settings.apps.indexOf(app)>-1){

		    if (this.getvar("__LIBRARIES__").hasOwnProperty("Root/"+app+"/static/"+name)){
		    	return this.getvar("__LIBRARIES__")["Root/"+app+"/static/"+name]
		    }
		    else{
		    	
		    	this.getvar("__LIBRARIES__")["Root/"+app+"/static/"+name]=require("Public/generates/generate_webcpack_require.js").load_library(app,name)
		    	return this.getvar("__LIBRARIES__")["Root/"+app+"/static/"+name]

		    }		    	

		      
		}
		else{
			warnings.push({"type":"no_app_actived",
						   "message":"La aplicacion "+app+" no esta activada",
						   "data":{"app":app}})
		}
		  
		    
		  
		
	}
	from_component(app,name){
		
		//carga el modulo de vue de cada aplicacion
		
		if ( settings.apps.indexOf(app)>-1){

		    if (this.getvar("__LIBRARIES__").hasOwnProperty("Root/"+app+"/static/components/"+name)){
		    	return this.getvar("__LIBRARIES__")["Root/"+app+"/static/components/"+name]
		    }
		    else{
		    	
		    	this.getvar("__LIBRARIES__")["Root/"+app+"/static/components/"+name]=require("Public/generates/generate_webcpack_require.js").load_library(app,"components/"+name)
		    	return this.getvar("__LIBRARIES__")["Root/"+app+"/static/components/"+name]

		    }		    	

		      
		}
		else{
			warnings.push({"type":"no_app_actived",
						   "message":"La aplicacion "+app+" no esta activada",
						   "data":{"app":app}})
		}
		  
		    
		  
		
	}
	setvar(name,value){
		window.__VUEAPP_VARIABLES__[name]=value
	}
	getvar(name){
		
		return window.__VUEAPP_VARIABLES__[name]
	}


	report_dependecies(){
		//envia la lista de dependecias encontradas (apps) que necesitan estar activadas para que la applicacion actual 
		//pueda funcionar correctamente, de modo que al saber esto el sistema pueda activar todas las dependencias cuando
		//cuando necesite activar esta
		
		fetch("/json/dashboard/report-dependecies", {
		   method: 'POST',
		   body: warnings
		})
		.then(function(response) {
		   if(response.ok) {
		       return response.text()
		   } else {
		       throw "Error en la llamada Ajax";
		   }

		})
		.then(function(texto) {
		   console.log(texto);
		})
		.catch(function(err) {
		   console.log(err);
		});
	}


}

VueApp.do_signal=do_signal
VueApp.add_signal=add_signal
export default VueApp



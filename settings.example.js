/*
Este script es usado para establecer variables de configuracion del sistemas
*/


module.exports={
	apps:["auth","dashboard"],
	mode:"dev",// dev | prod
	templates_folder:"views/",
	engine_view:"ejs",
	middlewares:[
	"auth/middlewares"
	],
	view_404:"error.ejs",
	input_login_with:"email", //email | nick | auto

	//central_addr = "http://localhost:8000/multiserver"
	//socket_options:{"query": {"port": 8000}}
	auto_routes:true,
	auto_apis:true,
	index_view:"index.ejs",
	login_view:"auth/login.ejs",
	profile_view:"auth/profile.ejs",
	register_view:"auth/register.ejs",
	reset_view:"auth/reset.ejs",

	logout_route:"/logout",
	login_redirect:"profile",
	logout_redirect:"login",
	reset_redirect:"reset",
	javascript_variables:{"base_url":"","templates_folder":"views/"}
}
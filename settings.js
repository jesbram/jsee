/*
Este script es usado para establecer variables de configuracion del sistemas
*/


module.exports={
	apps:["auth","dashboard","translation"],
	mode:"dev",// dev | prod
	templates_folder:"views/",
	engine_view:"ejs",
	middlewares:[
		"auth/middlewares",
		"dashboard/middlewares",
		"translation/middlewares"
	],
	view_404:"error.ejs",
	input_login_with:"email", //email | nick | auto

	//central_addr = "http://localhost:8000/multiserver"
	//socket_options:{"query": {"port": 8000}}
	auto_routes:true,
	auto_apis:true,
	auto_csrf:false,
	index_view:"index.ejs",
	login_view:"auth/login.ejs",
	profile_view:"auth/profile.ejs",
	register_view:"auth/register.ejs",
	reset_view:"auth/reset.ejs",
	resetPassword_view:"auth/resetPassword.ejs",
	
	logout_route:"/logout",
	login_redirect:"/profile",
	logout_redirect:"/login",
	javascript_variables:{"base_url":"","templates_folder":"views/"},
	vue_hooks:{
		"dashboard":[["auth","dashboard.vue"],//app, nombre del archivo de montaje.js
					],//son los ganchos de la app que aplicaciones cargan cosas como por ejemplo rutas de vue a la aplicacion hook
		},
	//igual que los vue_hook pero estas son para señales, trigger
	vue_signals:{
		"dashboard":[["auth","dashboard.js"],//app, nombre del archivo de montaje.js
					],
	},
	lang:"es"
}
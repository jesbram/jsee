//  webpack.config.js 
const settings=require("./settings.js")
const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
var entry={}
var static=[]
for (var elem of settings.apps){
    var app=require(path.resolve(__dirname,"./apps/"+elem+"/app.js"))
    app=new app(elem)
    static.push(path.resolve(__dirname,"apps/"+elem+"/static/"))
    if (app.settings){
        for (var elem2 in app.settings.compile){
            entry[elem+"-"+elem2]=path.resolve(__dirname,"apps/"+elem+"/"+app.settings.compile[elem2])
           
        }
    }
    

    
}

module.exports = [{
    
    entry:entry,
    mode:"development",
    output: {
        filename: 'js/dist/[name].js',
        path: path.resolve(__dirname, 'public/'),
        library: 'Level',
        publicPath: '/',
    },
    devtool: 'source-map',
    module: {
     
        rules: [
            {test: /\.vue$/,loader: 'vue-loader'},
            //{test:/\.js$/, loader:'babel', exclude: /node_modules/, query: { presets: ['es2015'] } },
            {test:/\.css$/, loader:'style!css', exclude: /node_modules/},
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
    
              },
              {test: /\.svg$/,loader: 'file-loader'},
            {test: /\.png$/,loader: 'file-loader'},
            {test: /\.token$/,loader: 'file-loader'},
              {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                      name: '[name].[ext]',
                      outputPath: 'fonts/'
                    }
                  }
                ]
              }

            ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    resolve: {
        alias: {
            "Public":path.resolve( __dirname, 'public'),
            "Root":path.resolve( __dirname,"apps"),
            "System":path.resolve( __dirname),
            'vue-full': 'vue/dist/vue.esm.js',
            "static":path.resolve( __dirname,"node_modules"),
           


        },
   
    },
}]